import os
import boto3
import logging

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

database = os.environ["DATABASE"]                                   # where2clean
flights_output_location = os.environ["FLIGHTS_OUTPUT_LOCATION"]     # s3://diva-dev-w2c-athena-query-results/flights/
gates_output_location = os.environ["GATES_OUTPUT_LOCATION"]         # s3://diva-dev-w2c-athena-query-results/gates/
workgroup = os.environ["WORKGROUP"]                                 # primary
source_s3_bucket_name = os.environ["SOURCE_S3_BUCKET_NAME"]         # diva-dev-w2c-athena-query-results

s3_client = boto3.client("s3")


def lambda_handler(event, context):

    # delete existing csv files in the S3 bucket
    delete_s3_objects()

    # run Athena queries
    athena = boto3.client('athena')

    # query flights data for the last 7 days
    flights_query_response = athena.start_query_execution(
        QueryString="SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition FROM (SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition, from_iso8601_timestamp(CONCAT(year,'-',month,'-',day,'T',hour,':00:00.000Z')) AS record_datetime from flights) WHERE record_datetime >= from_iso8601_timestamp(CONCAT(to_iso8601(current_date - interval '8' day),'T15:59:59.999Z'));",
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': flights_output_location,
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_S3'
            }
        },
        WorkGroup=workgroup
    )

    if len(flights_query_response) > 0:
        log.info("Flights query: Successful!")
        log.info("QueryExecutionId - {}".format(flights_query_response['QueryExecutionId']))
    else:
        log.info("Flights query: Failed!")

    # query gates data for the last 7 days
    gates_query_response = athena.start_query_execution(
        QueryString="SELECT current_gate, direction, flight_id, availability_status, flightno, display_datetime, gate_free_from, gate_free_till, last_cleaned_time, latest_cleaning_status, aircraft_type, arr_dep_flag, next_flight_id, shared_gate_flight_id, priority FROM (SELECT current_gate, direction, flight_id, availability_status, flightno, display_datetime, gate_free_from, gate_free_till, last_cleaned_time, latest_cleaning_status, aircraft_type, arr_dep_flag, next_flight_id, shared_gate_flight_id, priority, from_iso8601_timestamp(CONCAT(year,'-',month,'-',day,'T',hour,':00:00.000Z')) AS record_datetime from gates) WHERE record_datetime >= from_iso8601_timestamp(CONCAT(to_iso8601(current_date - interval '8' day),'T15:59:59.999Z'));",
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': gates_output_location,
            'EncryptionConfiguration': {
                'EncryptionOption': 'SSE_S3'
            }
        },
        WorkGroup=workgroup
    )

    if len(gates_query_response) > 0:
        log.info("Gates query: Successful!")
        log.info("QueryExecutionId - {}".format(gates_query_response['QueryExecutionId']))
    else:
        log.info("Gates query: Failed!")


def list_s3_objects():
    s3_objects_list = []

    # Create a reusable Paginator to search for more than 1000 objects
    paginator = s3_client.get_paginator('list_objects_v2')

    # Create a PageIterator from the Paginator 
    page_iterator = paginator.paginate(Bucket=source_s3_bucket_name) 
    
    for page in page_iterator:
        if 'Contents' in page:  # To check if the bucket has at least one object
            for obj in page['Contents']:
                key_str = obj['Key']

                s3_objects_list.append({"Key": key_str})

    return s3_objects_list


def delete_s3_objects():

    objects_list = list_s3_objects()

    if len(objects_list) > 0:
        s3_client.delete_objects(
            Bucket=source_s3_bucket_name,
            Delete={
                'Objects': objects_list,
                'Quiet': True
            }
        )