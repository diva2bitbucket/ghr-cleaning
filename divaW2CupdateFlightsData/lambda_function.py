import logging
import requests
import json
import os
import boto3
import time
import copy
import traceback
from base64 import b64encode
from base64 import b64decode
from datetime import datetime, timedelta
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)


class divaMSWrapper:
    
    """
    Class to call Flights Mircoservices
    """
    
    def __init__(self):
        
        #Details for Microservices API
        encrypted_api_ms_client_id = os.environ['API_MS_CLIENT_ID']
        self.api_ms_client_id = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encrypted_api_ms_client_id))['Plaintext'].decode('utf-8')
        
        encrypted_api_ms_client_secret = os.environ['API_MS_CLIENT_SECRET']
        self.api_ms_client_secret = boto3.client('kms').decrypt(CiphertextBlob=b64decode(encrypted_api_ms_client_secret))['Plaintext'].decode('utf-8')
        
        self.api_ms_authorize_url = os.environ['API_MS_AUTHORIZE_URL']                                                                  # https://apigateway-dev.changiairport.com/oauth2/accesstoken
        self.api_ms_arr_flight_changes_url = os.environ['API_MS_ARR_FLIGHT_CHANGES_URL'] + os.environ['API_MS_FLIGHT_CHANGES_WINDOW']   # https://apigateway-dev.changiairport.com/v1/flights/changes?direction=ARR&window=60
        self.api_ms_dep_flight_changes_url = os.environ['API_MS_DEP_FLIGHT_CHANGES_URL'] + os.environ['API_MS_FLIGHT_CHANGES_WINDOW']   # https://apigateway-dev.changiairport.com/v1/flights/changes?direction=DEP&window=60

        self.error_message = ''
        self.error_code = 0
        
        self.access_token = ''
        self.flight_info = ''
        self.flight_changes = ''
        self.arrival_flight_changes_status = ''
        self.departure_flight_changes_status = ''


    def authorize_diva_ms(self):
        
        """
        A method to authorize with Flights Microservices
        :return: True if authorization succeeds, otherwise an exception will be raised
        """
        
        try:
            payload = 'grant_type=client_credentials'
            
            string_authorization = self.api_ms_client_id + ':' + self.api_ms_client_secret
            encoded_authorization = b64encode(string_authorization.encode('ascii'))
            
            headers = {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic '+ encoded_authorization.decode('ascii')
            }
            
            response = requests.request("GET", self.api_ms_authorize_url, headers=headers, data=payload)

            if response.status_code == 200:
                self.access_token = json.loads(response.text)['access_token']
                response_json = json.loads(response.text)
                
                logging_json = {}
                logging_json["expires_in"] = response_json["expires_in"]
                logging_json["status"] = response_json["status"]
                
                log.info('DIVA MS GET Access Token API Success: %s %s' %(response.status_code, json.dumps(logging_json)))
                return True
            else:
                log.error('DIVA MS GET Access Token API Error: %s %s' %(response.status_code, response.text))
                self.error_message = response.text
                self.error_code = 400
                raise Exception
        
        except Exception as e:
            error_message = 'Error when getting DIVA MS access token'
            self.error_message = error_message
            self.error_code = 500
            
            log.error('%s, %s' %(error_message, e))
            log.error(traceback.format_exc())
            raise Exception (error_message)


    def get_departure_flight_changes(self, flight_changes_payload):
        
        """
        A method to get flight change information
        :param flight_changes_payload: payload used to call flights change microservices
        :return: True if retrieving flight change data succeeds, otherwise it will raise an exception
        """

        try:
            payload = flight_changes_payload
            headers = {
              'Authorization': 'Bearer ' + self.access_token,
              'Content-Type': 'text/plain'
            }
                
            response = requests.request("GET", self.api_ms_dep_flight_changes_url, headers=headers, data=json.dumps(payload))

            if response.status_code == 200:
                self.flight_changes = json.loads(response.text)
                log.info('DIVA MS GET Departure Flight Changes API Success: %s' %(response.status_code))
                log.info('Departure flight changes: {}'.format(self.flight_changes))
                if type(self.flight_changes) == dict:
                    if "message" in self.flight_changes["response"]:
                        if self.flight_changes["response"]["message"] == "No records found for this query":
                            log.info("No departure flight records.")
                            self.departure_flight_changes_status = "No departure flight records"
                            return False
                return True
            
            else:
                log.error('DIVA MS GET Departure Flight Changes API Error: %s %s' %(response.status_code, response.text))
                self.error_message = response.text
                self.error_code = 400
                raise Exception
                # return False
                
        except Exception as e:
            error_message = 'Error when getting DIVA MS Departure Flight Changes Data'
            self.error_message = error_message
            self.error_code = 500
            
            log.error('%s, %s' %(error_message, e))
            log.error(traceback.format_exc())
            raise Exception(error_message)
            # return False


    def get_arrival_flight_changes(self, flight_changes_payload):
        
        """
        A method to get flight change information
        :param flight_changes_payload: payload used to call flights change microservices
        :return: True if retrieving flight change data succeeds, otherwise it will raise an exception
        """

        try:
            payload = flight_changes_payload
            headers = {
              'Authorization': 'Bearer ' + self.access_token,
              'Content-Type': 'text/plain'
            }
                
            response = requests.request("GET", self.api_ms_arr_flight_changes_url, headers=headers, data=json.dumps(payload))

            if response.status_code == 200:
                self.flight_changes = json.loads(response.text)
                log.info('DIVA MS GET Arrival Flight Changes API Success: %s' %(response.status_code))
                log.info('Arrival flight changes: {}'.format(self.flight_changes))
                if type(self.flight_changes) == dict:
                    if "message" in self.flight_changes["response"]:
                        if self.flight_changes["response"]["message"] == "No records found for this query":
                            log.info("No arrival flight records.")
                            self.arrival_flight_changes_status = "No arrival flight records"
                            return False
                return True
            
            else:
                log.error('DIVA MS GET Arrival Flight Changes API Error: %s %s' %(response.status_code, response.text))
                self.error_message = response.text
                self.error_code = 400
                raise Exception
                # return False
                
        except Exception as e:
            error_message = 'Error when getting DIVA MS Arrival Flight Changes Data'
            self.error_message = error_message
            self.error_code = 500
            
            log.error('%s, %s' %(error_message, e))
            log.error(traceback.format_exc())
            raise Exception(error_message)
            # return False


class flightDataWrapper:
    
    """
    Class to handle flights data and update into Flights DynamoDB table
    """
    
    def __init__(self):

        self.dynamodb_client = boto3.client('dynamodb')
        self.dynamodb_resource = boto3.resource('dynamodb')
        self.s3_client = boto3.client("s3")
        self.flights_table = self.dynamodb_resource.Table(os.environ['DYNAMODB_FLIGHTS_TABLE'])     # diva-cleaning-flights
        self.flights_table_ = os.environ['DYNAMODB_FLIGHTS_TABLE']                                  # diva-cleaning-flights
        self.flights_ttl_duration = int(os.environ['FLIGHTS_TTL_DURATION'])                         # 172800 sec
        self.individual_gates_table = os.environ["DYNAMODB_INDIVIDUAL_GATES_TABLE"]                 # diva-cleaning-individual-gates
        self.individual_gates_ttl_duration = int(os.environ['INDIVIDUAL_GATES_TTL_DURATION'])       # 259200 sec
        self.mapping_files_bucket_name = os.environ['MAPPING_FILES_BUCKET_NAME']                    # diva-dev-cleaning-mapping-files
        self.gates_zone_list_file = os.environ['GATES_ZONE_LIST_FILE']                              # gates_zone_list.json
        self.date_today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        self.valid_gates = []

        self.s3_object = self.s3_client.get_object(Bucket=self.mapping_files_bucket_name, Key=self.gates_zone_list_file)
        self.gates_zone_list = json.loads(self.s3_object['Body'].read().decode('utf-8'))

        self.t123_departure_before_window = int(os.environ["T123_DEPARTURE_BEFORE_WINDOW"])          # 100 minutes
        self.t123_departure_after_window = int(os.environ["T123_DEPARTURE_AFTER_WINDOW"])            # 15 minutes
        self.t123_arrival_before_window = int(os.environ["T123_ARRIVAL_BEFORE_WINDOW"])              # 15 minutes <-
        self.t123_arrival_after_window = int(os.environ["T123_ARRIVAL_AFTER_WINDOW"])                # 35 minutes
        self.t4_departure_before_window = int(os.environ["T4_DEPARTURE_BEFORE_WINDOW"])              # 50 minutes
        self.t4_departure_after_window = int(os.environ["T4_DEPARTURE_AFTER_WINDOW"])                # 5 minutes
        self.t4_arrival_before_window = int(os.environ["T4_ARRIVAL_BEFORE_WINDOW"])                  # 10 minutes <-
        self.t4_arrival_after_window = int(os.environ["T4_ARRIVAL_AFTER_WINDOW"])                    # 20 minutes


    def prepare_dynamodb_flights_item(self, direction, flight_object):
        
        """
        A method to convert flight data into an object to be stored on DynamoDB
        :param fight_object: a single flight object from flight status or flight change data
        :return: the flight object that needs to be stored on DynamoDB
        """
        
        try:
            output = {}
            output['flight_id'] = direction + "-" + flight_object['flightno'] + "-" + flight_object['scheduled_date']
            output['direction'] = direction
            output['flightno'] = flight_object['flightno']
            output['scheduled_date'] = flight_object['scheduled_date']
            output['current_gate'] = flight_object['display_gate'] if direction == "ARR" else flight_object['current_gate']
            '''
            if output['current_gate'] == "":
                output['current_gate'] = flight_object['previous_gate']
            '''
            if output['current_gate'] != "":
                if output['current_gate'].find("R") >= 0:
                    output['current_gate'] = output['current_gate'].rstrip("R")
                elif output['current_gate'].find("L") >= 0:
                    output['current_gate'] = output['current_gate'].rstrip("L")
            output['previous_gate'] = flight_object['previous_gate']
            output['aircraft_type'] = flight_object['aircraft_type']
            output['arr_dep_flag'] = 'N'

            if flight_object['display_time'] != "":     # example: 0835
                hrs = flight_object['display_time'][:2]
                min = flight_object['display_time'][2:]
                output['display_datetime'] = flight_object['display_date'] + " " + hrs + ":" + min + ":00"
            else:
                output['display_datetime'] = flight_object['display_date'] + " " + flight_object['display_time']

            display_datetime = datetime.strptime(output['display_datetime'], "%Y-%m-%d %H:%M:%S")

            gate_zone_info = list(filter(lambda x: x['gate'] == output['current_gate'], self.gates_zone_list))

            if gate_zone_info[0]['terminal'] in [1, 2, 3]:
                if direction == 'DEP':
                    unavailable_start = display_datetime - timedelta(minutes=self.t123_departure_before_window)
                    unavailable_end = display_datetime + timedelta(minutes=self.t123_departure_after_window)
                elif direction == 'ARR':
                    unavailable_start = display_datetime - timedelta(minutes=self.t123_arrival_before_window)
                    unavailable_end = display_datetime + timedelta(minutes=self.t123_arrival_after_window)
            elif gate_zone_info[0]['terminal'] == 4:
                if direction == 'DEP':
                    unavailable_start = display_datetime - timedelta(minutes=self.t4_departure_before_window)
                    unavailable_end = display_datetime + timedelta(minutes=self.t4_departure_after_window)
                elif direction == 'ARR':
                    unavailable_start = display_datetime - timedelta(minutes=self.t4_arrival_before_window)
                    unavailable_end = display_datetime + timedelta(minutes=self.t4_arrival_after_window)

            output['unavailable_start'] = unavailable_start.strftime("%Y-%m-%d %H:%M:%S")
            output['unavailable_end'] = unavailable_end.strftime("%Y-%m-%d %H:%M:%S")
            output['message_timestamp'] = flight_object['message_timestamp']
            output['delete_marker'] = 'N'
            output['ttl'] = int(time.time() + self.flights_ttl_duration)        # epoch + 1 day (86400 sec TTL duration)
                    
            return output
            
        except Exception as e:
            error_message = 'Error when preparing DynamoDB flights table item'
            log.error('%s, %s' %(error_message, e))
            log.error(traceback.format_exc())
            raise Exception(error_message)


    def incremental_update_flights_db(self, direction, flight_data):
        
        """
        A method to perform incremental update to Flights DynamoDB table
        :param flight_data: flight data from either flight status or flight change microservices
        :return: True if incremental update succeeds, otherwise it will raise an exception
        """
        try:
            update_status = True
            for flight_object in flight_data:
                '''
                if flight_object['flight_type'] != "PAX" or (flight_object['display_gate'] == "" and flight_object['previous_gate'] == "") \
                    or flight_object['master_flt_no'] != "" or flight_object['display_date'] < self.date_today:
                    update_status = False
                    continue 
                '''
                if flight_object['flight_type'] == "PAX" and (flight_object['display_gate'] != "" if direction == "ARR" else flight_object['current_gate'] != "" or flight_object['previous_gate'] != "") and flight_object['master_flt_no'] == "" and flight_object['display_date'] >= self.date_today:
                    update_status = False
                    if flight_object['technical_flight_status2'].upper() in ["CAN", "DIV"]:     # Cancelled or Diverted
                        try:
                            update_type = "update_delete_marker"
                            update_status = self.update_flights_record(direction, flight_object, update_type)
                            #update_status = self.update_individual_gates_record(direction, flight_object, update_type)
                        except:
                            log.error("Error in setting delete marker on flight object: {}".format(flight_object))
                    elif flight_object['technical_flight_status2'].upper() in ["ARR", "DEP"]:
                        try:
                            update_type = "update_arr_dep_flag"
                            update_status = self.update_flights_record(direction, flight_object, update_type)
                        except:
                            log.error("Error in setting arr_dep_flag on flight object: {}".format(flight_object))
                    else:
                        update_type = "update_a_record"
                        update_status = self.update_flights_record(direction, flight_object, update_type)
                else:
                    continue

            return update_status
        
        except Exception as e:
            error_message = 'Error during incremental update of data in DynamoDB'
            log.exception('%s, %s %s' %(error_message, e, json.dumps(flight_object)))
            log.error(traceback.format_exc())
            raise Exception(error_message)


    def update_flights_record(self, direction, flight_object, update_type):
        
        """
        A helper method for incremental_update_flights_db to update records in Flights DynamoDB
        Records that did not fit the update criteria or encountered errors during deletion will be skipped
        :param fight_object: a single flight object from flight status or flight change data
        :return: True
        """
        try:
            dynamodb_record_exist = False
            update_item = self.prepare_dynamodb_flights_item(direction, flight_object)

            if update_item['current_gate'] in self.valid_gates:
                log.info("update_item - {}".format(update_item))
                log.info("update_type - {}".format(update_type))

                response = self.dynamodb_client.query(
                    TableName=self.flights_table_,
                    KeyConditionExpression='flight_id = :flight_id',
                    ExpressionAttributeValues={':flight_id': {'S': update_item['flight_id']}}
                )

                items = response['Items']
                if len(items) > 0:
                    dynamodb_record_exist = True
                else:
                    dynamodb_record_exist = False

                update_expression = 'SET direction = :direction, flightno = :flightno, scheduled_date = :scheduled_date, current_gate = :current_gate, previous_gate = :previous_gate, #arr_dep_flag = :arr_dep_flag, aircraft_type = :aircraft_type, display_datetime = :display_datetime, unavailable_start = :unavailable_start, unavailable_end = :unavailable_end, message_timestamp = :incoming_record_message_ts, #delete_marker = :delete_marker, #ttl = :ttl'

                if dynamodb_record_exist:
                    ttl_keys = items[0]['ttl'].keys()
                    if 'S' in ttl_keys:
                        ttl = int(items[0]['ttl']['S'])
                    elif 'N' in ttl_keys:
                        ttl = int(items[0]['ttl']['N'])
                    log.info("existing TTL - {}".format(ttl))
                    attribute_values = {
                        ':direction' : update_item['direction'],
                        ':flightno' : update_item['flightno'],
                        ':scheduled_date' : update_item['scheduled_date'],
                        ':current_gate' : update_item['current_gate'],
                        ':previous_gate' : update_item['previous_gate'],
                        ':arr_dep_flag' : 'Y' if update_type == "update_arr_dep_flag" else update_item['arr_dep_flag'],
                        ':aircraft_type' : update_item['aircraft_type'],
                        ':display_datetime': update_item['display_datetime'],
                        ':unavailable_start': update_item['unavailable_start'],
                        ':unavailable_end': update_item['unavailable_end'],
                        ':incoming_record_message_ts' : update_item['message_timestamp'],
                        ':delete_marker' : 'Y' if update_type == "update_delete_marker" else update_item['delete_marker'],
                        ':ttl' : ttl
                    }
                else:
                    attribute_values = {
                        ':direction' : update_item['direction'],
                        ':flightno' : update_item['flightno'],
                        ':scheduled_date' : update_item['scheduled_date'],
                        ':current_gate' : update_item['current_gate'],
                        ':previous_gate' : update_item['previous_gate'],
                        ':arr_dep_flag' : 'Y' if update_type == "update_arr_dep_flag" else update_item['arr_dep_flag'],
                        ':aircraft_type' : update_item['aircraft_type'],
                        ':display_datetime': update_item['display_datetime'],
                        ':unavailable_start': update_item['unavailable_start'],
                        ':unavailable_end': update_item['unavailable_end'],
                        ':incoming_record_message_ts' : update_item['message_timestamp'],
                        ':delete_marker' : 'Y' if update_type == "update_delete_marker" else update_item['delete_marker'],
                        ':ttl' : update_item['ttl']
                    }

                attribute_names = {'#delete_marker' : 'delete_marker', '#arr_dep_flag' : 'arr_dep_flag', '#ttl' : 'ttl'}

                if update_type == "update_delete_marker":
                    condition_expression = 'message_timestamp < :incoming_record_message_ts OR attribute_not_exists(message_timestamp)'
                else:
                    condition_expression = '(message_timestamp < :incoming_record_message_ts OR attribute_not_exists(message_timestamp)) OR \
                        (current_gate <> :current_gate OR attribute_not_exists(current_gate)) OR \
                        (previous_gate <> :previous_gate OR attribute_not_exists(previous_gate)) OR \
                        (aircraft_type <> :aircraft_type OR attribute_not_exists(aircraft_type)) OR \
                        (display_datetime <> :display_datetime OR attribute_not_exists(display_datetime))'
                
                try:
                    self.flights_table.update_item(
                        Key = {'flight_id': update_item['flight_id']},
                        UpdateExpression = update_expression,
                        ConditionExpression = condition_expression,
                        ExpressionAttributeValues = attribute_values,
                        ExpressionAttributeNames = attribute_names
                    )
                    log.info('Updated data into DynamoDB for this record: %s' %(json.dumps(flight_object)))
                except self.dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException:
                    return False
                except Exception as e:
                    log.exception(e)
                    return False
            else:
                log.error('Invalid current_gate - {}'.format(update_item['current_gate']))
                return False
        except:
            log.error('Internal error when trying to update this record and it was skipped: %s' %(json.dumps(flight_object)))
            log.error(traceback.format_exc())
        return True


    def update_individual_gates_record(self, direction, flight_object, update_type):

        new_flight_id = ""
        new_display_datetime = ""
        new_flightno = ""
        new_direction = ""

        individual_gates_table_ = self.dynamodb_resource.Table(self.individual_gates_table)
        current_gate = flight_object['display_gate'] if direction == "ARR" else flight_object['current_gate']
        attribute_values = {':current_gate': {'S': current_gate}, ':N': {'S': 'N'}}

        if update_type == "update_delete_marker":
            response = self.dynamodb_client.query(
                TableName=self.individual_gates_table,
                KeyConditionExpression='current_gate = :current_gate',
                ExpressionAttributeValues=attribute_values,
                FilterExpression='arr_dep_flag = :N'
            )

            if len(response['Items']) > 0:
                items = response['Items']
                if items[0]['flight_id'] == flight_object['flight_id']:
                    if items[0]['next_flight_id']['S'] != "" and items[0]['next_flight_display_datetime']['S'] != "":
                        new_flight_id = items[0]['next_flight_id']['S']
                        new_display_datetime = items[0]['next_flight_display_datetime']['S']
                        new_flightno = new_flight_id.split("-")[1]
                        new_direction = items[0]['next_flight_id']['S'][:3]     # get the three first characters e.g. 'ARR' or 'DEP'

                        update_individual_gate = {
                            'direction': new_direction,
                            'current_gate': current_gate,
                            'flight_id': new_flight_id,
                            'flightno': new_flightno,
                            'arr_dep_flag': items[0]['arr_dep_flag']['S'],
                            'display_datetime': new_display_datetime, 
                            'aircraft_type': items[0]['aircraft_type']['S'],
                            'current_flight_unavailable_start': '',
                            'current_flight_unavailable_end': '',
                            'next_flight_id': '',
                            'next_flight_display_datetime': '',
                            'next_flight_unavailable_start': '',
                            'next_flight_unavailable_end': '',
                            'last_cleaned_time': items[0]['last_cleaned_time']['S'],
                            'priority': 1,
                            'latest_cleaning_status': items[0]['latest_cleaning_status']['S'],
                            'availability_status': items[0]['availability_status']['S'],
                            'gate_group': '',
                            'ttl' : self.individual_gates_ttl_duration
                        }

                        try:
                            update_expression = 'SET '
                            attribute_values = {}
                            attribute_names = {}
                            counter = 1

                            for key in update_individual_gate:
                                if key != 'current_gate':
                                    var = ':var' + str(counter)
                                    
                                    # To handle updates of DynamoDB for using 'ttl' as a reserved name
                                    if key == 'ttl':
                                        new_key = '#ttl'
                                        attribute_names[new_key] = key
                                        update_expression = update_expression + new_key + '= ' + var + ', '
                                    else:
                                        update_expression = update_expression + key + '= ' + var + ', '
                                        
                                    attribute_values[var] = update_individual_gate[key]
                                    counter += 1
                                    
                            update_expression = update_expression[:-2]
                            try:
                                individual_gates_table_.update_item(
                                    Key = {'current_gate': update_individual_gate['current_gate']},
                                    UpdateExpression = update_expression,
                                    ExpressionAttributeValues = attribute_values,
                                    ExpressionAttributeNames = attribute_names)
                                log.info('Updated data into DynamoDB for this record: %s' %(json.dumps(update_individual_gate)))
                            except self.dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
                                log.exception(ccfe)
                                pass
                        except:
                            log.error('Internal error when trying to update this record and it was skipped: %s' %(json.dumps(update_individual_gate)))
                            log.error(traceback.format_exc())
                    else:
                        try:
                            individual_gates_table_.delete_item(
                                Key={'current_gate': current_gate}
                            )
                            log.info('Deleted DynamoDB Invididual Gate record: %s' %(json.dumps({'current_gate': current_gate})))
                        except self.dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
                            log.exception(ccfe)
                            pass
                        except Exception:
                            log.error('Internal error when trying to delete this record and it was skipped: %s' %(json.dumps({'current_gate': current_gate})))
                            log.error(traceback.format_exc())
        return True


    def create_valid_gates_list(self):

        for gate_info in self.gates_zone_list:
            self.valid_gates.append(gate_info['gate'])


def lambda_handler(event, context):
    
    flight_changes_payload = {
	    "role": "elevated"
    }

    DIVA_API = divaMSWrapper()
    flight_wrapper = flightDataWrapper()

    if DIVA_API.authorize_diva_ms():
        flight_wrapper.create_valid_gates_list()
        # Departure flights
        if DIVA_API.get_departure_flight_changes(flight_changes_payload):
            direction = 'DEP'
            flight_data = DIVA_API.flight_changes
            if flight_wrapper.incremental_update_flights_db(direction, flight_data):
                log.info("Successful update to DynamoDB Flights table")
            else:
                log.info("No updates to DynamoDB Flights table")
        # Arrival flights
        if DIVA_API.get_arrival_flight_changes(flight_changes_payload):
            direction = 'ARR'
            flight_data = DIVA_API.flight_changes
            if flight_wrapper.incremental_update_flights_db(direction, flight_data):
                log.info("Successful update to DynamoDB Flights table")
            else:
                log.info("No updates to DynamoDB Flights table")
                    
    if DIVA_API.arrival_flight_changes_status == 'No arrival flight records' and DIVA_API.departure_flight_changes_status == 'No departure flight records':
        log.info("No arrival or departure flight records to process")