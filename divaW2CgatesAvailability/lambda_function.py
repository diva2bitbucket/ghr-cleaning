import logging
import boto3
import json
import os
import time
import traceback
from datetime import datetime, timedelta
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

gates_table = os.environ["DYNAMODB_GATES_TABLE"]                                        # diva-cleaning-gates
gates_ttl_duration = int(os.environ['GATES_TTL_DURATION'])                              # 259200 sec
individual_gates_table = os.environ['DYNAMODB_INDIVIDUAL_GATES_TABLE']                  # diva-cleaning-individual-gates
individual_gates_ttl_duration = int(os.environ['INDIVIDUAL_GATES_TTL_DURATION'])        # 259200 sec
t3_gates = os.environ['T3_GATES'].strip().split(",")                                    # A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15,A16,A17,A18,A19,A20,A21,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10

dynamodb_resource = boto3.resource('dynamodb')
dynamodb_client = boto3.client("dynamodb")
s3_client = boto3.client("s3")

gates_table_ = dynamodb_resource.Table(gates_table)
individual_gates_table_ = dynamodb_resource.Table(individual_gates_table)

datetime_format = "%Y-%m-%d %H:%M:%S"

def lambda_handler(event, context):
    """
    This lambda function determines the availability of gates.
    1. It will loop through all the records in the individual gates DynamoDB table.
    2. For each record, it will determine the value for availability_status field.
    3. Then store each updated record in individual_gates_record_list array.
    4. Update individual gates table with updated gate record
    5. Send updated gate record to Kinesis Data Firehose for storage in S3
    """
    individual_gates_record_list = []
    temp_individual_gate = {}
    ttl = 0
    priority = 0
    new_flight_id = ""
    new_flight_display_datetime = ""
    new_flight_unavailable_start = ""
    new_flight_unavailable_end = ""
    new_flightno = ""
    new_direction = ""
    new_aircraft_type = ""
    change_flag = False

    attribute_values = {':blank': {'S': ''}}

    response = dynamodb_client.scan(
        TableName=individual_gates_table,
        ExpressionAttributeValues=attribute_values,
        FilterExpression='display_datetime <> :blank'
    )

    if len(response['Items']) > 0:
        gate_items = response['Items']

        for item in gate_items:
            current_gate = item['current_gate']['S']
            log.info("current_gate - {}".format(current_gate))
            direction = item['direction']['S']
            log.info("direction - {}".format(direction))
            availability_status = item['availability_status']['S']
            latest_cleaning_status = item['latest_cleaning_status']['S']

            item_keys = item.keys()
            if 'priority' in item_keys:
                priority_keys = item['priority'].keys()
                if 'S' in priority_keys:
                    priority = int(item['priority']['S']) if item['priority']['S'] != "" else 1
                elif 'N' in priority_keys:
                    priority = int(item['priority']['N']) if item['priority']['N'] != 0 else 1
            
            next_flight_unavailable_start = datetime.strptime(item['next_flight_unavailable_start']['S'], datetime_format) if item['next_flight_unavailable_start']['S'] != "" else ""
            current_datetime = datetime.now() + timedelta(hours=8)

            if next_flight_unavailable_start != "":
                if latest_cleaning_status != "CLEANING" and current_datetime >= next_flight_unavailable_start:
                    new_flight_id = item['next_flight_id']['S']
                    new_flight_display_datetime = item['next_flight_display_datetime']['S']
                    new_flight_unavailable_start = item['next_flight_unavailable_start']['S']
                    new_flight_unavailable_end = item['next_flight_unavailable_end']['S']
                    new_flightno = new_flight_id.split("-")[1]
                    new_direction = item['next_flight_id']['S'][:3]     # get the three first characters e.g. 'ARR' or 'DEP'
                    new_aircraft_type = item['next_flight_aircraft_type']['S']
                    priority += 1
                    ttl = int(time.time() + gates_ttl_duration)
                    change_flag = True

            if ttl == 0:
                ttl_keys = item['ttl'].keys()
                if 'S' in ttl_keys:
                    ttl = int(item['ttl']['S'])
                elif 'N' in ttl_keys:
                    ttl = int(item['ttl']['N'])    

            temp_individual_gate = {
                'current_gate': item['current_gate']['S'],
                'direction': new_direction if new_direction != "" else item['direction']['S'],
                'flight_id': new_flight_id if new_flight_id != "" else item['flight_id']['S'],
                'flightno': new_flightno if new_flightno != "" else item['flightno']['S'],
                'arr_dep_flag': 'N' if new_flight_id != "" else item['arr_dep_flag']['S'],
                'display_datetime': new_flight_display_datetime if new_flight_display_datetime != "" else item['display_datetime']['S'],
                'aircraft_type': new_aircraft_type if new_aircraft_type != "" else item['aircraft_type']['S'],
                'current_flight_unavailable_start': new_flight_unavailable_start if new_flight_unavailable_start != "" else item['current_flight_unavailable_start']['S'],
                'current_flight_unavailable_end': new_flight_unavailable_end if new_flight_unavailable_end != "" else item['current_flight_unavailable_end']['S'],
                'next_flight_id': "" if new_flight_id != "" else item['next_flight_id']['S'],
                'next_flight_display_datetime': "" if new_flight_display_datetime != "" else item['next_flight_display_datetime']['S'],
                'next_flight_unavailable_start': "" if new_flight_unavailable_start != "" else item['next_flight_unavailable_start']['S'],
                'next_flight_unavailable_end': "" if new_flight_unavailable_end != "" else item['next_flight_unavailable_end']['S'],
                'next_flight_aircraft_type': "" if new_aircraft_type != "" else item['next_flight_aircraft_type']['S'],
                'last_cleaned_time': item['last_cleaned_time']['S'],
                'priority': priority if priority != 0 else "",
                'latest_cleaning_status': latest_cleaning_status,
                'availability_status': '',
                'gate_group': item['gate_group']['S'],
                'ttl': ttl
            }

            current_flight_unavailable_end = datetime.strptime(temp_individual_gate['current_flight_unavailable_end'], datetime_format)

            if latest_cleaning_status != "CLEANING":
                if latest_cleaning_status == "CLEANED" and availability_status != "CLEANED":
                    temp_individual_gate['availability_status'] = latest_cleaning_status
                    change_flag = True

                if current_datetime < current_flight_unavailable_end and latest_cleaning_status != 'NOT AVAILABLE':
                    if temp_individual_gate['current_gate'] in t3_gates and temp_individual_gate['direction'] == 'ARR':
                        temp_individual_gate['latest_cleaning_status'] = 'AVAILABLE'
                        temp_individual_gate['availability_status'] = 'AVAILABLE'
                    else:
                        temp_individual_gate['latest_cleaning_status'] = 'NOT AVAILABLE'
                        temp_individual_gate['availability_status'] = 'NOT AVAILABLE - CURRENT FLIGHT'
                    change_flag = True
                else:
                    if temp_individual_gate['arr_dep_flag'] == 'N' and latest_cleaning_status != 'NOT AVAILABLE':
                        temp_individual_gate['latest_cleaning_status'] = 'NOT AVAILABLE'
                        temp_individual_gate['availability_status'] = 'NOT AVAILABLE - CURRENT FLIGHT'
                        change_flag = True
                    elif temp_individual_gate['arr_dep_flag'] == 'N' and latest_cleaning_status == 'NOT AVAILABLE':
                        temp_individual_gate['latest_cleaning_status'] = 'NOT AVAILABLE'
                        temp_individual_gate['availability_status'] = 'NOT AVAILABLE - CURRENT FLIGHT'
                        change_flag = True
                    elif temp_individual_gate['arr_dep_flag'] == 'Y' and latest_cleaning_status != 'AVAILABLE' and current_datetime > current_flight_unavailable_end:
                        temp_individual_gate['latest_cleaning_status'] = 'AVAILABLE'
                        temp_individual_gate['availability_status'] = 'AVAILABLE'
                        change_flag = True

            if change_flag:
                individual_gates_record_list.append(temp_individual_gate)

            temp_individual_gate = {}
            ttl = 0
            priority = 0
            new_flight_id = ""
            new_flight_display_datetime = ""
            new_flight_unavailable_start = ""
            new_flight_unavailable_end = ""
            new_flightno = ""
            new_direction = ""
            new_aircraft_type = ""
            change_flag = False

        if len(individual_gates_record_list) > 0:
            log.info("individual_gates_record_list count - {}".format(len(individual_gates_record_list)))
            for individual_gate_record_ in individual_gates_record_list:

                update_individual_gates_record(individual_gate_record_)

            individual_gates_record_list = []


def update_individual_gates_record(update_individual_gate):
	
    try:
        update_expression = 'SET '
        attribute_values = {}
        attribute_names = {}
        counter = 1

        for key in update_individual_gate:
            if key != 'current_gate':
                var = ':var' + str(counter)
				
                # To handle updates of DynamoDB for using 'ttl' as a reserved name
                if key == 'ttl':
                    new_key = '#ttl'
                    attribute_names[new_key] = key
                    update_expression = update_expression + new_key + ' = ' + var + ', '
                else:
                    update_expression = update_expression + key + ' = ' + var + ', '
					
                attribute_values[var] = update_individual_gate[key]
                counter += 1
				
        update_expression = update_expression[:-2]
        try:
            individual_gates_table_.update_item(
                Key = {'current_gate': update_individual_gate['current_gate']},
                UpdateExpression = update_expression,
                ExpressionAttributeValues = attribute_values,
                ExpressionAttributeNames = attribute_names)

            log.info('Updated data into DynamoDB Individual Gate table for this record: %s' %(json.dumps(update_individual_gate)))

        except dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
            log.exception(ccfe)
            pass
    except:
        log.error('Internal error when trying to update this record and it was skipped: %s' %(json.dumps(update_individual_gate)))
        log.error(traceback.format_exc())

    return True