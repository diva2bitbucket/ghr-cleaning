# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import boto3
import json
import re
import logging
import time
import mysql.connector
from decimal import Decimal
from datetime import datetime, timedelta

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = (
    "[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)"
)
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)


class CheckSensorData():
    
    def __init__(self):
        
        self.dynamodb_resource = boto3.resource('dynamodb')
        self.dynamodb_client = boto3.client("dynamodb")
        self.s3_client = boto3.client("s3")

        self.host = os.environ['HOST'].strip()                                                  # cag-people-counting.cb9yb3bx0mcs.ap-southeast-1.rds.amazonaws.com
        self.username = os.environ['USERNAME'].strip()                                          # AdminCAGDB
        self.password = os.environ['PASSWORD'].strip()                                          # cagDB2021!
        self.database = os.environ['DATABASE'].strip()                                          # nexwahDB
        self.nex_device_table = os.environ['NEX_DEVICE_TABLE'].strip()                          # Nex_Device
        self.nex_device_data_table = os.environ['NEX_DEVICE_DATA_TABLE'].strip()                # Nex_DeviceData

        self.facilities_master_db_table = self.dynamodb_resource.Table(os.environ["DYNAMODB_FACILITIES_MASTER_TABLE"].strip())          # diva-cleaning-facilities-master
        self.facilities_job_db_table = self.dynamodb_resource.Table(os.environ["DYNAMODB_FACILITIES_JOB_TABLE"].strip())                # diva-cleaning-facilities-job
        self.parameter_db_table = self.dynamodb_resource.Table(os.environ["DYNAMODB_PARAMETER_TABLE"].strip())                          # diva-cleaning-parameter
        self.parameter_db_table_ = os.environ["DYNAMODB_PARAMETER_TABLE"].strip()                                                       # diva-cleaning-parameter

        self.traffic_type_high = int(os.environ['TRAFFIC_TYPE_HIGH'].strip())                   # e.g. 60
        self.traffic_type_medium = int(os.environ['TRAFFIC_TYPE_MEDIUM'].strip())               # e.g. 40
        self.traffic_type_low = int(os.environ['TRAFFIC_TYPE_LOW'].strip())                     # e.g. 20

        self.ttl_duration = int(os.environ['TTL_DURATION'])                                     # 259200 (sec)
        self.cleaning_sla = int(os.environ['CLEANING_SLA'].strip())                             # 20 (min)

        self.s3_key_prefix = os.environ['S3_KEY_PREFIX'].strip()                                # raw-sensor-data/
        self.s3_bucket_name = os.environ['S3_BUCKET_NAME'].strip()                              # diva-dev-raw-sensor-data
        self.s3_filename_prefix = os.environ['S3_FILENAME_PREFIX'].strip()                      # raw-sensor-data-
        self.s3_file_extension = os.environ['S3_FILE_EXTENSION'].strip()                        # json

        self.now_timestamp = datetime.now() + timedelta(hours=8)
        self.now_datetime = datetime.strftime(self.now_timestamp, "%Y-%m-%d %H:%M:%S")


    def get_db_connection(self):
        """
        Function to establish connection to the database.
        
        """

        log.info("LOG: Inside get_db_connection method...")

        conn = mysql.connector.connect(user=self.username, password=self.password, host=self.host, database=self.database)

        return conn


    def get_sensor_data_query_time(self):

        log.info("LOG: Inside get_sensor_data_query method...")
        
        sensor_data_query_time = ''

        response = self.dynamodb_client.scan(TableName=self.parameter_db_table_)

        if len(response['Items']) > 0:
            log.info("Items - %s" % response['Items'])
            item_keys = response['Items'][0].keys()
            if 'sensor_data_query_time' in item_keys:
                sensor_data_query_time = response['Items'][0]['sensor_data_query_time']['S']
                if sensor_data_query_time == '':
                    sensor_data_query_time = datetime.strftime(self.now_timestamp - timedelta(minutes=2), "%Y-%m-%d %H:%M:%S")
            else:
                sensor_data_query_time = datetime.strftime(self.now_timestamp - timedelta(minutes=2), "%Y-%m-%d %H:%M:%S")
        else:
            sensor_data_query_time = datetime.strftime(self.now_timestamp - timedelta(minutes=2), "%Y-%m-%d %H:%M:%S")

        log.info("LOG: sensor_data_query_time - %s" % sensor_data_query_time)

        return sensor_data_query_time


    def execute_sql_statement(self):

        log.info("LOG: Inside execute_sql_statement method...")

        for_insert = False
        raw_sensor_data = []
        sensor_data = {}
        s3_object_key = ''
        traffic_type = ''
        pax_traffic_count = 0
        target_cleaned_time = datetime.strftime(self.now_timestamp + timedelta(minutes=self.cleaning_sla), "%Y-%m-%d %H:%M:%S")

        sensor_data_query_time = self.get_sensor_data_query_time()

        if sensor_data_query_time != '':

            statement = "SELECT SUBSTRING_INDEX(c.DeviceName,'(',1) AS DeviceName, c.DeviceId, c.InData, c.OutData, c.receiveTime FROM (SELECT a.DeviceName, a.DeviceId, b.InData, b.OutData, b.receiveTime FROM nexwahDB.Nex_Device a, (SELECT * FROM nexwahDB.Nex_DeviceData WHERE receiveTime BETWEEN '" + sensor_data_query_time + "' AND '" + self.now_datetime + "' AND InData = 1) b WHERE a.DeviceId = b.DeviceId AND SUBSTRING(a.DeviceName,1,1) = 'T') c;"

            log.info("LOG: SQL statement - {}".format(statement))
            query = (statement)
            log.info("LOG: Start query...")
            self.cursor.execute(query)
            log.info("LOG: End query...")

            for i in self.cursor:

                log.info("Raw sensor data - {}".format(i))

                receiveTime = i['receiveTime'].strftime('%Y-%m-%d %H:%M:%S')

                response = self.facilities_master_db_table.query(
                    KeyConditionExpression='facility_code = :facility_code',
                    ExpressionAttributeValues={':facility_code': i['DeviceName']}
                )

                items = response['Items']
                #log.info("Facilities master DB items - {}".format(items))
                if len(items) > 0:
                    item_keys = items[0].keys()

                    traffic_type = items[0]['traffic_type']
                    facility_name = items[0]['facility_name']
                    facility_type = items[0]['facility_type']
                    terminal = items[0]['terminal']
                    level = items[0]['level']
                    public_transit = items[0]['public_transit']
                    toilet_type = items[0]['toilet_type']
                    assigned_callsign = items[0]['assigned_callsign']
                    if 'pax_traffic_count' in item_keys:
                        pax_traffic_count = int(items[0]['pax_traffic_count']) + 1
                    else:
                        pax_traffic_count = 1

                    self.facilities_master_db_table.update_item(
                        Key = {'facility_code': i['DeviceName']},
                        UpdateExpression = 'SET pax_traffic_count = :pax_traffic_count',
                        ExpressionAttributeValues = {':pax_traffic_count': pax_traffic_count}
                    )

                    self.parameter_db_table.update_item(
                        Key = {'id': 1},
                        UpdateExpression = 'SET sensor_data_query_time = :sensor_data_query_time',
                        ExpressionAttributeValues = {':sensor_data_query_time': receiveTime}
                    )

                    if traffic_type == 'High':
                        if pax_traffic_count >= self.traffic_type_high:

                            self.facilities_master_db_table.update_item(
                                Key = {'facility_code': i['DeviceName']},
                                UpdateExpression = 'SET job_trigger_time = :job_trigger_time',
                                ExpressionAttributeValues = {':job_trigger_time': self.now_datetime}
                            )

                            response1 = self.facilities_job_db_table.query(
                                KeyConditionExpression='facility_code = :facility_code',
                                ExpressionAttributeValues={':facility_code': i['DeviceName']}
                            )

                            items1 = response1['Items']
                            log.info("Facilities job DB items - {}".format(items1))

                            if len(items1) > 0:

                                if items1[0]['latest_cleaning_status'] not in ['AVAILABLE', 'CLEANING']:
                                    ttl_ = int(items1[0]['ttl'])
                                    job_trigger_time = items1[0]['job_trigger_time']

                                    self.facilities_job_db_table.delete_item(
                                        Key = {'facility_code': i['DeviceName'], 'job_trigger_time': job_trigger_time}
                                    )

                                    for_insert = True
                            else:
                                ttl_ = int(time.time()) + self.ttl_duration
                                for_insert = True

                            if for_insert:
                                self.facilities_job_db_table.put_item(
                                    Item={'facility_code': i['DeviceName'],
                                        'job_trigger_time': self.now_datetime,
                                        'target_cleaned_time': target_cleaned_time,
                                        'latest_cleaning_status': 'AVAILABLE',
                                        'fault_type': 'High Pax Traffic',
                                        'priority': 1,
                                        'facility_name': facility_name,
                                        'facility_type': facility_type,
                                        'terminal': terminal,
                                        'level': level,
                                        'public_transit': public_transit,
                                        'toilet_type': toilet_type,
                                        'assigned_callsign': assigned_callsign,
                                        'ttl': ttl_
                                    }
                                )
                                for_insert = False

                    elif traffic_type == 'Medium':
                        if pax_traffic_count >= self.traffic_type_medium:

                            self.facilities_master_db_table.update_item(
                                Key = {'facility_code': i['DeviceName']},
                                UpdateExpression = 'SET job_trigger_time = :job_trigger_time',
                                ExpressionAttributeValues = {':job_trigger_time': self.now_datetime}
                            )

                            response1 = self.facilities_job_db_table.query(
                                KeyConditionExpression='facility_code = :facility_code',
                                ExpressionAttributeValues={':facility_code': i['DeviceName']}
                            )

                            items1 = response1['Items']
                            log.info("Facilities job DB items - {}".format(items1))

                            if len(items1) > 0:

                                if items1[0]['latest_cleaning_status'] not in ['AVAILABLE', 'CLEANING']:
                                    ttl_ = int(items1[0]['ttl'])
                                    job_trigger_time = items1[0]['job_trigger_time']

                                    self.facilities_job_db_table.delete_item(
                                        Key = {'facility_code': i['DeviceName'], 'job_trigger_time': job_trigger_time}
                                    )

                                    for_insert = True
                            else:
                                ttl_ = int(time.time()) + self.ttl_duration
                                for_insert = True

                            if for_insert:
                                self.facilities_job_db_table.put_item(
                                    Item={'facility_code': i['DeviceName'],
                                        'job_trigger_time': self.now_datetime,
                                        'target_cleaned_time': target_cleaned_time,
                                        'latest_cleaning_status': 'AVAILABLE',
                                        'fault_type': 'High Pax Traffic',
                                        'priority': 1,
                                        'facility_name': facility_name,
                                        'facility_type': facility_type,
                                        'terminal': terminal,
                                        'level': level,
                                        'public_transit': public_transit,
                                        'toilet_type': toilet_type,
                                        'assigned_callsign': assigned_callsign,
                                        'ttl': ttl_
                                    }
                                )
                                for_insert = False

                    elif traffic_type == 'Low':
                        if pax_traffic_count >= self.traffic_type_low:

                            self.facilities_master_db_table.update_item(
                                Key = {'facility_code': i['DeviceName']},
                                UpdateExpression = 'SET job_trigger_time = :job_trigger_time',
                                ExpressionAttributeValues = {':job_trigger_time': self.now_datetime}
                            )

                            response1 = self.facilities_job_db_table.query(
                                KeyConditionExpression='facility_code = :facility_code',
                                ExpressionAttributeValues={':facility_code': i['DeviceName']}
                            )

                            items1 = response1['Items']
                            log.info("Facilities job DB items - {}".format(items1))

                            if len(items1) > 0:

                                if items1[0]['latest_cleaning_status'] not in ['AVAILABLE', 'CLEANING']:
                                    ttl_ = int(items1[0]['ttl'])
                                    job_trigger_time = items1[0]['job_trigger_time']

                                    self.facilities_job_db_table.delete_item(
                                        Key = {'facility_code': i['DeviceName'], 'job_trigger_time': job_trigger_time}
                                    )

                                    for_insert = True
                            else:
                                ttl_ = int(time.time()) + self.ttl_duration
                                for_insert = True

                            if for_insert:
                                self.facilities_job_db_table.put_item(
                                    Item={'facility_code': i['DeviceName'],
                                        'job_trigger_time': self.now_datetime,
                                        'target_cleaned_time': target_cleaned_time,
                                        'latest_cleaning_status': 'AVAILABLE',
                                        'fault_type': 'High Pax Traffic',
                                        'priority': 1,
                                        'facility_name': facility_name,
                                        'facility_type': facility_type,
                                        'terminal': terminal,
                                        'level': level,
                                        'public_transit': public_transit,
                                        'toilet_type': toilet_type,
                                        'assigned_callsign': assigned_callsign,
                                        'ttl': ttl_
                                    }
                                )
                                for_insert = False

                sensor_data['DeviceName'] = i['DeviceName']
                sensor_data['DeviceId'] = i['DeviceId']
                sensor_data['InData'] = i['InData']
                sensor_data['OutData'] = i['OutData']
                sensor_data['receiveTime'] = i['receiveTime'].strftime('%Y-%m-%d %H:%M:%S')

                raw_sensor_data.append(sensor_data.copy())
                sensor_data = {}

            s3_key_prefix = self.s3_key_prefix
            s3_object_key += s3_key_prefix + self.now_timestamp.strftime('%Y/%m/%d') + '/'
            s3_object_key += self.s3_filename_prefix + self.now_timestamp.strftime('%Y-%m-%d_%H-%M-%S')
            s3_object_key += '.' + self.s3_file_extension

            log.info('LOG: s3 path - s3://%s/%s' % (self.s3_bucket_name, s3_object_key))

            response = self.s3_client.put_object(
                Body=json.dumps(raw_sensor_data),
                Bucket=self.s3_bucket_name,
                Key=s3_object_key
            )

        if self.cursor.rowcount > 0:
            log.info("Number of records retrieved - {}".format(self.cursor.rowcount))


    # MAIN METHOD
    def main(self):

        log.info("LOG: Inside main method...")

        try:
            self.conn = self.get_db_connection()
            self.cursor = self.conn.cursor(dictionary=True)

            self.execute_sql_statement()

            self.cursor.close()
            self.conn.close()
        except Exception as e:
            log.error(e)
        finally:
            self.cursor.close()
            self.conn.close()


def lambda_handler(event, context):

    main_class = CheckSensorData()
    main_class.main()