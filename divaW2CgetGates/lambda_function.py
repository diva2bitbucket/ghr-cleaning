import os
import json
import boto3
import logging
import traceback
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)


def lambda_handler(event, context):
    """
    Send all gates records to the frontend
    """
    logging.info(event)
    # verify user performing event request
    # TODO
    log.info("event - {}".format(json.dumps(event)))
    req_body = json.loads(event["body"])
    access_token = req_body["access_token"]

    cognito_idp = boto3.client("cognito-idp")
    try:
        cog_response = cognito_idp.get_user(AccessToken=access_token)
    except cognito_idp.exceptions.NotAuthorizedException as e:
        return {'statusCode': 401} # to stop further processing
    except Exception as e:
        log.error("Unknown error while checking auth: {}".format(str(e)))
    else:
        log.info("Cognito call successful, user is authorised; response: {}".format(cog_response))

    # request
    ddb_client = boto3.client("dynamodb")
    table_name = os.environ["DYNAMODB_GATES_TABLE"]
    try:
        response = ddb_client.scan(TableName=table_name,)
    except ClientError as e:
        log.error(e)
        # return different status code
        # TODO
        return {
            'statusCode': 500,
            'body': json.dumps({
                'message_type': 'getGates',
                'message': 'Unknown error occurred while getting items in database. Please check logs.'
            })
        }
    else:
        log.info("Scan successful: {}".format(response))
    
    # return 200 if working
    cleaned_response = []
    try: 
        items = response["Items"]
        for item in items:
            for attribute in item:
                if "S" in item[attribute]:
                    item[attribute] = item[attribute]["S"]
                elif "N" in item[attribute]:
                    item[attribute] = int(item[attribute]["N"])
                elif "NULL" in item[attribute]:
                    item[attribute] = ""
                elif "BOOL" in item[attribute]:
                    item[attribute] = item[attribute]["BOOL"]
            cleaned_response.append(item)
    except Exception as e:
        log.error("Error in trimming dynamoDB response: {}".format(str(e)))
        return {
            'statusCode': 500,
            'body': json.dumps({
                'message_type': 'getGates',
                'message': 'Error in trimming db response'
            })
        }
        
    try:
        temp = {
            'statusCode': 200,
            'body': json.dumps({
                'message_type': 'getGates',
                'data': cleaned_response
            })
        }
    except Exception as e:
        log.error('%s' %(e))
        log.error(traceback.format_exc())

    log.info(temp)

    return temp