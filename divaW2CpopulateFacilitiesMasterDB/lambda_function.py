import os
import json
import boto3
import logging

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

dynamodb_resource = boto3.resource('dynamodb')
dynamodb_client = boto3.client("dynamodb")
s3_client = boto3.client("s3")

facilities_master_db_table = dynamodb_resource.Table(os.environ["DYNAMODB_FACILITIES_MASTER_TABLE"])            # diva-cleaning-facilities-master
mapping_files_bucket_name = os.environ['MAPPING_FILES_BUCKET_NAME']                                             # diva-dev-cleaning-mapping-files
list_of_facilities_file = os.environ['LIST_OF_FACILITIES_FILE']                                                 # list_of_facilities.json

s3_object = s3_client.get_object(Bucket=mapping_files_bucket_name, Key=list_of_facilities_file)
facilities = json.loads(s3_object['Body'].read().decode('utf-8'))


def lambda_handler(event, context):

    try:
        if len(facilities) > 0:

            for facility in facilities:

                if facility["facility_code"] == '':
                    continue

                facility_code = facility["facility_code"]
                facility_name = facility["location_name"]
                facility_type = facility["facility_type"]
                terminal = facility["terminal"]
                level = facility["level"]
                public_transit = facility["public_transit"]
                toilet_type = facility["toilet_type"]
                traffic_type = facility["traffic_type"]
                assigned_callsign = facility["assigned_callsign"]

                update_expression = 'SET facility_name = :facility_name, facility_type = :facility_type, terminal = :terminal, #level = :level, public_transit = :public_transit, toilet_type = :toilet_type, traffic_type = :traffic_type, assigned_callsign = :assigned_callsign'

                attribute_values = {
                    ':facility_name' : facility_name,
                    ':facility_type' : facility_type,
                    ':terminal' : terminal,
                    ':level' : level,
                    ':public_transit' : public_transit,
                    ':toilet_type' : toilet_type,
                    ':traffic_type' : traffic_type,
                    ':assigned_callsign' : assigned_callsign
                }

                attribute_names = {'#level' : 'level'}

                facilities_master_db_table.update_item(
                    Key = {'facility_code': facility_code},
                    UpdateExpression = update_expression,
                    ExpressionAttributeValues = attribute_values,
                    ExpressionAttributeNames = attribute_names
                )

                #log.info('Updated facilities master table for facility code: %s' %(facility_code))
            response = {'statusCode': 200, 'body': json.dumps({'status': 200})}
    except Exception as e: 
        log.info("facilities master table was not updated for facility code due to an exception.")
        log.exception(e)
        response = {'statusCode': 500, 'body': json.dumps({'ERROR': 'Update failed!'})}
    
    return response