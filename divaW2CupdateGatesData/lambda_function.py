import logging
import boto3
import json
import os
import time
import traceback
from datetime import datetime, timedelta
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

gates_ttl_duration = int(os.environ['GATES_TTL_DURATION'])                                                  # 259200 sec
gates_table = os.environ["DYNAMODB_GATES_TABLE"]                                                            # diva-cleaning-gates
individual_gates_table_gate_group_index = os.environ['DYNAMODB_INDIVIDUAL_GATES_TABLE_GATE_GROUP_INDEX']    # diva-cleaning-individual-gates-gate_group-index
individual_gates_table = os.environ["DYNAMODB_INDIVIDUAL_GATES_TABLE"]                                      # diva-cleaning-individual-gates
mapping_files_bucket_name = os.environ['MAPPING_FILES_BUCKET_NAME']                                         # diva-dev-cleaning-mapping-files
gates_zone_list_file = os.environ['GATES_ZONE_LIST_FILE']                                                   # gates_zone_list.json
t3_gates = os.environ['T3_GATES'].strip().split(",")                                                        # A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15,A16,A17,A18,A19,A20,A21,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10

dynamodb_resource = boto3.resource('dynamodb')
dynamodb_client = boto3.client("dynamodb")
s3_client = boto3.client("s3")

gates_table_ = dynamodb_resource.Table(gates_table)

s3_object = s3_client.get_object(Bucket=mapping_files_bucket_name, Key=gates_zone_list_file)
gates_zone_list = json.loads(s3_object['Body'].read().decode('utf-8'))

temp_gate = {}
gate_record = {}
dependencies_list = []
datetime_format = "%Y-%m-%d %H:%M:%S"


def lambda_handler(event, context):
    log.info('event: ' + json.dumps(event, indent=2))
    
    # Compact batches - remove redundant batches or batches with deleteRecords
    event_records = compact_event(event)
    log.info('event_records: {}'.format(event_records))
    if event_records == []:
        log.info("Zero event records, aborting")
        return

    update_gates_table(event_records)


def compact_event(event):
    """
    Compacts events - remove all deleted records (if there is any)
    :event dict: event object received by lambda
    :return event_records: compacted events to pass to front end
    """

    compacted_events = []
    event_records = event['Records']

    for event_record in event_records:
        dynamodb_keys = event_record['dynamodb'].keys()
        if 'NewImage' in dynamodb_keys:
            for field in event_record['dynamodb']["NewImage"]:
                if "S" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = event_record['dynamodb']["NewImage"][field]["S"]
                elif "N" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = int(event_record['dynamodb']["NewImage"][field]["N"])
                elif "NULL" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = ""
                elif "BOOL" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = event_record['dynamodb']["NewImage"][field]["BOOL"]
        if 'OldImage' in dynamodb_keys:
            for field in event_record['dynamodb']["OldImage"]:
                if "S" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = event_record['dynamodb']["OldImage"][field]["S"]
                elif "N" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = int(event_record['dynamodb']["OldImage"][field]["N"])
                elif "NULL" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = ""
                elif "BOOL" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = event_record['dynamodb']["OldImage"][field]["BOOL"]
        compacted_events.append(event_record)

    return compacted_events


def update_gates_table(event_records):
    """
    Update gates dynamodb table
    :event_records: compacted event records, returned by compact_event method
    1. create a list (array) of unique current_gate for deleted records in individual gates table; source data is event_records
    2. create a list (array) of unique current_gate for individual gates in individual gates table; source data is event_records
    3. create a list (array) of unique gate_group for gate groups in individual gates table; source data is event_records
    4. loop on the deleted_gates_list, for each record: delete the corresponding record in gates table
    5. loop on the individual_gates_list, for each record: fill-in the corresponding fields in the temp_gate object and update gates table
    5. loop on the shared_gates_list, for each record: fill-in the corresponding fields in the temp_gate object, check for flight(s) that is/are blocking the current gate, then update gates table
    """
    if len(event_records) > 0:

        temp_gate = {
            'current_gate': '',
            'direction': '',
            'flight_id': '',
            'flightno': '',
            'arr_dep_flag': '',
            'display_datetime': '', 
            'aircraft_type': '',
            'next_flight_id': '',
            'availability_status': '',
            'last_cleaned_time': '',
            'latest_cleaning_status': '',
            'shared_gate_flight_id': '',
            'unavailable_shared_gate': '',
            'gate_free_from': '',
            'gate_free_till': '',
            'ttl' : int(time.time() + gates_ttl_duration)
        }

        deleted_gates_list = []
        invididual_gates_list = []
        shared_gates_list = []

        for event in event_records:
            gate_info = {'gate_group': '-', 'current_gate': 'X'}
            dynamodb_keys = event['dynamodb'].keys()
            if 'NewImage' in dynamodb_keys:
                gate_info = event['dynamodb']['NewImage']
            else:
                if 'OldImage' in dynamodb_keys:
                    gate_info = event['dynamodb']['OldImage']

            if event['eventName'] == 'REMOVE':
                deleted_gate = event['dynamodb']['Keys']['current_gate']['S']
                if deleted_gate not in deleted_gates_list:
                    deleted_gates_list.append(deleted_gate)
            else:   # eventName is either MODIFY or INSERT
                if gate_info['gate_group'] == "-":
                    if gate_info['current_gate'] not in invididual_gates_list:
                        invididual_gates_list.append(gate_info['current_gate'])
            if gate_info['gate_group'] != "-":
                if gate_info['gate_group'] not in shared_gates_list:
                    shared_gates_list.append(gate_info['gate_group'])

        # deleted gates
        if len(deleted_gates_list) > 0:
            for gate_ in deleted_gates_list:
                try:
                    gates_table_.delete_item(
                        Key={'current_gate': gate_}
                    )
                    log.info('Deleted DynamoDB Gate record: %s' %(json.dumps({'current_gate': gate_})))
                except dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
                    log.exception(ccfe)
                    pass
                except Exception:
                    log.error('Internal error when trying to delete this record and it was skipped: %s' %(json.dumps({'current_gate': gate_})))
                    log.error(traceback.format_exc())

        # individual gates
        if len(invididual_gates_list) > 0:

            for gate_ in invididual_gates_list:

                attribute_values = {':current_gate': {'S': gate_}}

                response = dynamodb_client.query(
                    TableName=individual_gates_table,
                    KeyConditionExpression='current_gate = :current_gate',
                    ExpressionAttributeValues=attribute_values
                )

                if len(response['Items']) > 0:
                    individual_gate = response['Items'][0]

                    temp_gate['current_gate'] = individual_gate['current_gate']['S']
                    temp_gate['direction'] = individual_gate['flight_id']['S'][:3]
                    temp_gate['flight_id'] = individual_gate['flight_id']['S']
                    temp_gate['flightno'] = individual_gate['flightno']['S']
                    temp_gate['arr_dep_flag'] = individual_gate['arr_dep_flag']['S']
                    temp_gate['display_datetime'] = individual_gate['display_datetime']['S']
                    temp_gate['aircraft_type'] = individual_gate['aircraft_type']['S']
                    temp_gate['next_flight_id'] = individual_gate['next_flight_id']['S']
                    temp_gate['availability_status'] = individual_gate['availability_status']['S']
                    temp_gate['last_cleaned_time'] = individual_gate['last_cleaned_time']['S']
                    temp_gate['latest_cleaning_status'] = individual_gate['latest_cleaning_status']['S']
                    individual_gate_keys = individual_gate.keys()
                    if 'priority' in individual_gate_keys:
                        priority_keys = individual_gate['priority'].keys()
                        if 'S' in priority_keys:
                            priority = int(individual_gate['priority']['S'])
                        elif 'N' in priority_keys:
                            priority = int(individual_gate['priority']['N'])
                        temp_gate['priority'] = priority

                    if individual_gate['next_flight_id']['S'] == "":
                        if individual_gate['current_flight_unavailable_end']['S'] != "":
                            temp_gate['gate_free_from'] = individual_gate['current_flight_unavailable_end']['S']
                            temp_gate['gate_free_till'] = 'TBA'
                    else:
                        if individual_gate['current_flight_unavailable_end']['S'] != "" and individual_gate['next_flight_unavailable_start']['S'] != "":
                            c_f_u_e = individual_gate['current_flight_unavailable_end']['S']
                            current_flight_unavailable_end = datetime.strptime(c_f_u_e, datetime_format)
                            n_f_u_s = individual_gate['next_flight_unavailable_start']['S']
                            next_flight_unavailable_start = datetime.strptime(n_f_u_s, datetime_format)

                            if current_flight_unavailable_end < next_flight_unavailable_start:
                                temp_gate['gate_free_from'] = individual_gate['current_flight_unavailable_end']['S']
                                temp_gate['gate_free_till'] = individual_gate['next_flight_unavailable_start']['S']
                            else:
                                temp_gate['gate_free_from'] = individual_gate['next_flight_unavailable_end']['S']
                                temp_gate['gate_free_till'] = 'TBA'

                    update_gates_dynamodb_table(temp_gate)

        # shared gates group
        if len(shared_gates_list) > 0:

            flights_list = []
            flight_info = {}
            gate_free_from = ""
            gate_free_till = ""
            free_from = datetime.now()
            free_till = ""
            shared_gate_flight_id = ""
            shared_gate_direction = ""
            unavailable_shared_gate = ""
            now = datetime.now() + timedelta(hours=8)

            for gate_group in shared_gates_list:

                has_not_available_gate = False

                attribute_values1 = {':gate_group': {'S': gate_group}}

                query_result = dynamodb_client.query(
                    TableName=individual_gates_table,
                    IndexName=individual_gates_table_gate_group_index,
                    KeyConditionExpression='gate_group = :gate_group',
                    ExpressionAttributeValues=attribute_values1
                )

                if len(query_result['Items']) > 0:
                    gates = query_result['Items']

                    for gate in gates:
                        current_flight_unavailable_start = datetime.strptime(gate['current_flight_unavailable_start']['S'], datetime_format)

                        if not has_not_available_gate:
                            if gate['latest_cleaning_status']['S'] == 'NOT AVAILABLE' and current_flight_unavailable_start < now:
                                shared_gate_flight_id = gate["flight_id"]['S']
                                shared_gate_direction = gate["direction"]['S']
                                unavailable_shared_gate = gate['current_gate']['S']
                                has_not_available_gate = True
                        
                        if gate['flight_id']['S'] != "":
                            unavailable_start = datetime.strptime(gate['current_flight_unavailable_start']['S'], datetime_format)
                            unavailable_end = datetime.strptime(gate['current_flight_unavailable_end']['S'], datetime_format)

                            flight_info['flight_id'] = gate['flight_id']['S']
                            flight_info['unavailable_start'] = unavailable_start
                            flight_info['unavailable_end'] = unavailable_end
                            flights_list.append(flight_info.copy())
                            flight_info = {}

                        if gate['next_flight_id']['S'] != "":
                            unavailable_start = datetime.strptime(gate['next_flight_unavailable_start']['S'], datetime_format)
                            unavailable_end = datetime.strptime(gate['next_flight_unavailable_end']['S'], datetime_format)

                            flight_info['flight_id'] = gate['next_flight_id']['S']
                            flight_info['unavailable_start'] = unavailable_start
                            flight_info['unavailable_end'] = unavailable_end
                            flights_list.append(flight_info.copy())
                            flight_info = {}

                    sorted_flights_list = sorted(flights_list, key = lambda x: x["unavailable_start"])

                    for gate in gates:

                        availability_status = ''
                        latest_cleaning_status = gate['latest_cleaning_status']['S']

                        temp_gate['current_gate'] = gate['current_gate']['S']
                        temp_gate['direction'] = gate['flight_id']['S'][:3]
                        temp_gate['flight_id'] = gate['flight_id']['S']
                        temp_gate['flightno'] = gate['flightno']['S']
                        temp_gate['arr_dep_flag'] = gate['arr_dep_flag']['S']
                        temp_gate['display_datetime'] = gate['display_datetime']['S']
                        temp_gate['aircraft_type'] = gate['aircraft_type']['S']
                        temp_gate['next_flight_id'] = gate['next_flight_id']['S']
                        temp_gate['last_cleaned_time'] = gate['last_cleaned_time']['S']
                        gate_keys = gate.keys()
                        if 'priority' in gate_keys:
                            priority_keys = gate['priority'].keys()
                            if 'S' in priority_keys:
                                priority = int(gate['priority']['S'])
                            elif 'N' in priority_keys:
                                priority = int(gate['priority']['N'])
                            temp_gate['priority'] = priority

                        if gate['current_flight_unavailable_end']['S'] != "":
                            current_flight_unavailable_end = datetime.strptime(gate['current_flight_unavailable_end']['S'], datetime_format)
                            free_from = max(current_flight_unavailable_end, now)
                        else:
                            free_from = now
                        
                        free_till = ""

                        if len(sorted_flights_list) > 0:
                            for flight in sorted_flights_list:
                                if free_from < flight['unavailable_end']:
                                    if flight['unavailable_start'] <= free_from:
                                        free_from = flight['unavailable_end']
                                    else:
                                        free_till = flight['unavailable_start']
                                        break

                        if gate['latest_cleaning_status']['S'] == 'AVAILABLE' and has_not_available_gate:
                            latest_cleaning_status = 'NOT AVAILABLE'
                            availability_status = 'NOT AVAILABLE - SHARED GATE'
                        elif gate['latest_cleaning_status']['S'] == 'NOT AVAILABLE' and temp_gate['flight_id'] == shared_gate_flight_id:
                            latest_cleaning_status = 'NOT AVAILABLE'
                            availability_status = 'NOT AVAILABLE - CURRENT FLIGHT'
                        elif gate['latest_cleaning_status']['S'] == 'NOT AVAILABLE' and has_not_available_gate:
                            latest_cleaning_status = 'NOT AVAILABLE'
                            availability_status = 'NOT AVAILABLE - SHARED GATE'
                        else:
                            latest_cleaning_status = gate['latest_cleaning_status']['S']
                            availability_status = gate['availability_status']['S']

                        if type(free_from) == datetime:
                            gate_free_from = free_from.strftime(datetime_format)

                        if type(free_till) == datetime:
                            gate_free_till = free_till.strftime(datetime_format)
                        else:
                            gate_free_till = 'TBA'

                        temp_gate['gate_free_from'] = gate_free_from
                        temp_gate['gate_free_till'] = gate_free_till

                        if unavailable_shared_gate in t3_gates and shared_gate_direction == 'ARR':
                            temp_gate['shared_gate_flight_id'] = shared_gate_flight_id
                            latest_cleaning_status = 'AVAILABLE'
                            availability_status = 'AVAILABLE'

                        temp_gate['availability_status'] = availability_status
                        temp_gate['latest_cleaning_status'] = latest_cleaning_status

                        if availability_status == 'NOT AVAILABLE - SHARED GATE':
                            temp_gate['shared_gate_flight_id'] = shared_gate_flight_id
                            temp_gate['unavailable_shared_gate'] = unavailable_shared_gate
                            
                        update_gates_dynamodb_table(temp_gate)
                        temp_gate = {
                            'current_gate': '',
                            'direction': '',
                            'flight_id': '',
                            'flightno': '',
                            'arr_dep_flag': '',
                            'display_datetime': '', 
                            'aircraft_type': '',
                            'next_flight_id': '',
                            'availability_status': '',
                            'last_cleaned_time': '',
                            'latest_cleaning_status': '',
                            'shared_gate_flight_id': '',
                            'unavailable_shared_gate': '',
                            'gate_free_from': '',
                            'gate_free_till': '',
                            'ttl' : int(time.time() + gates_ttl_duration)
                        }
                flights_list = []
                flight_info = {}
                gate_free_from = ""
                gate_free_till = ""
                free_till = ""
                shared_gate_flight_id = ""
                shared_gate_direction = ""
                unavailable_shared_gate = ""


def update_gates_dynamodb_table(update_individual_gate):
	
    try:
        update_expression = 'SET '
        attribute_values = {}
        attribute_names = {}
        counter = 1

        for key in update_individual_gate:
            if key != 'current_gate':
                var = ':var' + str(counter)
				
                # To handle updates of DynamoDB for using 'ttl' as a reserved name
                if key == 'ttl':
                    new_key = '#ttl'
                    attribute_names[new_key] = key
                    update_expression = update_expression + new_key + ' = ' + var + ', '
                else:
                    update_expression = update_expression + key +  ' = ' + var + ', '
					
                attribute_values[var] = update_individual_gate[key]
                counter += 1
				
        update_expression = update_expression[:-2]
        try:
            gates_table_.update_item(
                Key = {'current_gate': update_individual_gate['current_gate']},
                UpdateExpression = update_expression,
                ExpressionAttributeValues = attribute_values,
                ExpressionAttributeNames = attribute_names)

            log.info('Updated data into DynamoDB Gates table for this record: %s' %(json.dumps(update_individual_gate)))

        except dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
            log.exception(ccfe)
            pass
    except:
        log.error('Internal error when trying to update this record and it was skipped: %s' %(json.dumps(update_individual_gate)))
        log.error(traceback.format_exc())

    return True