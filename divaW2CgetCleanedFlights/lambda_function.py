import os
import json
import boto3
import logging
import traceback
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

s3_resource = boto3.resource('s3')

mapping_files_source_bucket = os.environ['MAPPING_FILES_SOURCE_BUCKET']    # diva-dev-cleaning-mapping-files
terminal_gates_json_file = os.environ['TERMINAL_GATES_JSON_FILE']          # terminal_gates.json

object_content = s3_resource.Object(mapping_files_source_bucket, terminal_gates_json_file)
file_content = object_content.get()['Body'].read().decode('utf-8')
terminal_gates = json.loads(file_content)

terminal_1_gates = terminal_gates['Terminal 1']
terminal_2_gates = terminal_gates['Terminal 2']
terminal_3_gates = terminal_gates['Terminal 3']
terminal_4_gates = terminal_gates['Terminal 4']


def lambda_handler(event, context):
    """
    Send all cleaned flight records to the frontend
    """
    logging.info(event)
    # verify user performing event request
    # TODO
    log.info("event - {}".format(json.dumps(event)))
    req_body = json.loads(event["body"])
    access_token = req_body["access_token"]

    cognito_idp = boto3.client("cognito-idp")
    try:
        cog_response = cognito_idp.get_user(AccessToken=access_token)
    except cognito_idp.exceptions.NotAuthorizedException as e:
        return {'statusCode': 401} # to stop further processing
    except Exception as e:
        log.error("Unknown error while checking auth: {}".format(str(e)))
    else:
        log.info("Cognito call successful, user is authorised; response: {}".format(cog_response))

    '''
    cleaned_flight = {
        "arr_dep_flag": "",
        "shared_gate_flight_id": "",
        "flight_id": "",
        "unavailable_shared_gate": "",
        "priority": 0,
        "ttl": 0,
        "aircraft_type": "",
        "display_datetime": "",
        "gate_free_from": "",
        "latest_cleaning_status": "CLEANED",
        "flightno": "",
        "gate_free_till": "",
        "direction": "",
        "current_gate": "",
        "next_flight_id": "",
        "availability_status": "CLEANED",
        "last_cleaned_time": ""
    }
    '''

    cleaned_flight = {
        "flight_id": "",
        "current_gate": "",
        "terminal": "",
        "flightno": "",
        "direction": "",
        "user_id": "",
        "latest_cleaning_status": "",
        "availability_status": "",
        "last_cleaned_start": "",
        "last_cleaned_time": "",
        "cleaning_status": "",
        "ceiling_finishes_good_condition": "",
        "wall_finishes_good_condition": "",
        "floor_finishes_good_condition": "",
        "seats_good_condition": "",
        "bins_good_condition": "",
        "down_ramp_good_condition": "",
        "other_fixtures_good_condition": "",
        "water_cooler_good_condition": "",
        "air_con_good_condition": "",
        "hand_railings_good_condition": "",
        "high_touch_points_good_condition": "",
        "counters_good_condition": ""
    }

    # request
    ddb_client = boto3.client("dynamodb")
    table_name = os.environ["DYNAMODB_FLIGHTS_TABLE"]
    try:
        attribute_values = {':COMPLETE': {'S': 'COMPLETE'}}
        response = ddb_client.scan(
            TableName=table_name, 
            ExpressionAttributeValues=attribute_values, 
            FilterExpression='cleaning_status = :COMPLETE'
        )
    except ClientError as e:
        log.error(e)
        # return different status code
        # TODO
        return {
            'statusCode': 500,
            'body': json.dumps({
                'message_type': 'getCleanedFlights',
                'message': 'Unknown error occurred while getting items in database. Please check logs.'
            })
        }
    else:
        log.info("Scan successful: {}".format(response))
    
    # return 200 if working
    cleaned_flights = []
    try: 
        items = response["Items"]
        if len(items) > 0:
            for item in items:
                for attribute in item:
                    if "S" in item[attribute]:
                        item[attribute] = item[attribute]["S"]
                    elif "N" in item[attribute]:
                        item[attribute] = int(item[attribute]["N"])
                    elif "NULL" in item[attribute]:
                        item[attribute] = ""
                    elif "BOOL" in item[attribute]:
                        item[attribute] = item[attribute]["BOOL"]

                '''
                cleaned_flight['flight_id'] = item['flight_id']
                cleaned_flight['flightno'] = item['flightno']
                cleaned_flight['direction'] = item['direction']
                cleaned_flight['current_gate'] = item['current_gate']
                cleaned_flight['last_cleaned_time'] = item['last_cleaned_complete']
                '''

                item_keys = item.keys()

                if item['current_gate'] in terminal_3_gates:
                    terminal = "Terminal 3",
                elif item['current_gate'] in terminal_2_gates:
                    terminal = "Terminal 2"
                elif item['current_gate'] in terminal_1_gates:
                    terminal = "Terminal 1"
                elif item['current_gate'] in terminal_4_gates:
                    terminal = "Terminal 4"

                cleaned_flight = {
                    "flight_id": item['flight_id'],
                    "current_gate": item['current_gate'],
                    "terminal": terminal,
                    "flightno": item['flightno'],
                    "direction": item['direction'],
                    "user_id": item['user_id'],
                    "latest_cleaning_status": "CLEANED",
                    "availability_status": "CLEANED",
                    "last_cleaned_start": item['last_cleaned_start'],
                    "last_cleaned_time": item['last_cleaned_complete'],
                    "cleaning_status": item['cleaning_status'],
                    "ceiling_finishes_good_condition": item['ceiling_finishes_good_condition'],
                    "wall_finishes_good_condition": item['wall_finishes_good_condition'],
                    "floor_finishes_good_condition": item['floor_finishes_good_condition'],
                    "seats_good_condition": item['seats_good_condition'],
                    "bins_good_condition": item['bins_good_condition'],
                    "down_ramp_good_condition": item['down_ramp_good_condition'],
                    "other_fixtures_good_condition": item['other_fixtures_good_condition'],
                    "water_cooler_good_condition": item['water_cooler_good_condition'],
                    "air_con_good_condition": item['air_con_good_condition'],
                    "hand_railings_good_condition": item['hand_railings_good_condition'],
                    "high_touch_points_good_condition": item['high_touch_points_good_condition'] if 'high_touch_points_good_condition' in item_keys else "",
                    "counters_good_condition": item['counters_good_condition'] if 'counters_good_condition' in item_keys else ""
                }

                cleaned_flights.append(cleaned_flight.copy())

                '''
                cleaned_flight = {
                    "arr_dep_flag": "",
                    "shared_gate_flight_id": "",
                    "flight_id": "",
                    "unavailable_shared_gate": "",
                    "priority": 0,
                    "ttl": 0,
                    "aircraft_type": "",
                    "display_datetime": "",
                    "gate_free_from": "",
                    "latest_cleaning_status": "CLEANED",
                    "flightno": "",
                    "gate_free_till": "",
                    "direction": "",
                    "current_gate": "",
                    "next_flight_id": "",
                    "availability_status": "CLEANED",
                    "last_cleaned_time": ""
                }
                '''

                cleaned_flight = {
                    "flight_id": "",
                    "current_gate": "",
                    "terminal": "",
                    "flightno": "",
                    "direction": "",
                    "user_id": "",
                    "latest_cleaning_status": "",
                    "availability_status": "",
                    "last_cleaned_start": "",
                    "last_cleaned_time": "",
                    "cleaning_status": "",
                    "ceiling_finishes_good_condition": "",
                    "wall_finishes_good_condition": "",
                    "floor_finishes_good_condition": "",
                    "seats_good_condition": "",
                    "bins_good_condition": "",
                    "down_ramp_good_condition": "",
                    "other_fixtures_good_condition": "",
                    "water_cooler_good_condition": "",
                    "air_con_good_condition": "",
                    "hand_railings_good_condition": "",
                    "high_touch_points_good_condition": "",
                    "counters_good_condition": ""
                }

        else:
            return {
                'statusCode': 500,
                'body': json.dumps({
                    'message_type': 'getCleanedFlights',
                    'message': 'No cleaned flight records found.'
                })
            }
    except Exception as e:
        log.error("Error in trimming dynamoDB response: {}".format(str(e)))
        return {
            'statusCode': 500,
            'body': json.dumps({
                'message_type': 'getCleanedFlights',
                'message': 'Error in trimming db response'
            })
        }
        
    try:
        temp = {
            'statusCode': 200,
            'body': json.dumps({
                'message_type': 'getCleanedFlights',
                'data': cleaned_flights
            })
        }
    except Exception as e:
        log.error('%s' %(e))
        log.error(traceback.format_exc())

    log.info(temp)

    return temp