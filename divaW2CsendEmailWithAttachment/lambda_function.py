import os
import glob
import boto3
import logging
from subprocess import call
from datetime import date, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

source_s3_bucket_name = os.environ["SOURCE_S3_BUCKET_NAME"]         # diva-prod-cleaning-logs-extraction-results
sender_email_address = os.environ["SENDER_EMAIL_ADDRESS"]
recipients_email_address = os.environ["RECIPIENTS_EMAIL_ADDRESS"]
aws_region = os.environ["REGION"]

DOWNLOAD_LOCATION_PATH = "/tmp/"
keys_list = []
list_of_files = []

s3_resource = boto3.resource("s3")
s3_client = boto3.client("s3")


def lambda_handler(event, context):

    process_csv_files()


def get_from_and_to_date():

    run_date = date.today()
    from_date = run_date - timedelta(days=7)
    to_date = run_date

    return from_date.strftime('%Y-%m-%d'), to_date.strftime('%Y-%m-%d')


def local_directory():
    '''
    1. Create a local directory inside Lambda to store the ZIP files temporarily 
    2. Usually temporary files are stored in "/tmp" location within Lambda 
    '''
        
    # Temporary use of non-persistent disk space in Lambda's own '/tmp' directory for downloading files from source S3

    if not os.path.exists(DOWNLOAD_LOCATION_PATH):
        log.info("Creating local directory /tmp to store the files")
        os.mkdir(DOWNLOAD_LOCATION_PATH)
    else:
        log.info('tmp directory already exists, clean it up before re-using')
        call('rm -rf /tmp/*', shell=True)


def download_files_from_source_s3():
    # Create local directory inside Lambda if it does not exist 
    local_directory()
    
    #from_date, to_date = get_from_and_to_date()
        
    # Connect to S3 bucket
    source_s3_bucket = s3_resource.Bucket(source_s3_bucket_name)
    
    # Create a reusable Paginator to search for more than 1000 objects
    paginator = s3_client.get_paginator('list_objects_v2')

    # Create a PageIterator from the Paginator 
    page_iterator = paginator.paginate(Bucket=source_s3_bucket_name) 

    #page_iterator = paginator.paginate(Bucket=self.source_s3_bucket_name, StartAfter=self.search_start_after)

    for page in page_iterator:
        if 'Contents' in page:  # To check if the bucket has at least one object
            for obj in page['Contents']:
                key_str = obj['Key']

                # Look for keys that ends with .csv
                if key_str.endswith(".csv"):

                    key_str_filename = key_str

                    '''
                    if key_str.find("flights") >= 0:
                        key_str_filename = "where2clean_flights_data_" + from_date + "_to_" + to_date + ".csv"
                    elif key_str.find("gates") >= 0:
                        key_str_filename = "where2clean_gates_" + from_date + "_to_" + to_date + ".csv"
                    '''

                    csv_temp_path = DOWNLOAD_LOCATION_PATH + key_str_filename

                    # Download S3 object to a file
                    source_s3_bucket.meta.client.download_file(source_s3_bucket_name, key_str, csv_temp_path)
                    
                    # Append all the ZIP files to a list
                    keys_list.append(obj['Key'])
                    list_of_files = glob.glob(os.path.join(DOWNLOAD_LOCATION_PATH, "*"))
        else:
            # No objects found
            log.info('No files found in source S3 bucket')
    
    return list_of_files


def process_csv_files():

    subject = ""
    recipients = []

    if recipients_email_address.find(",") >= 0:
        recipients = recipients_email_address.split(",")
    else:
        recipients = recipients_email_address

    list_of_csv_files = download_files_from_source_s3()

    if len(list_of_csv_files) > 0:

        for csv_file in list_of_csv_files:
            if csv_file.find("flights") >= 0:
                subject = "Where2Clean flights data from last 7 days"

                if len(recipients) > 0:
                    for recipient in recipients:
                        send_email(sender_email_address, recipient, aws_region, subject, csv_file)
                else:
                    send_email(sender_email_address, recipients, aws_region, subject, csv_file)
            '''
            elif csv_file.find("gates") >= 0:
                subject = "Where2Clean gates data from last 7 days"
            '''


def send_email(sender, recipient, aws_region, subject, file_name):

    csv_filename = file_name[5:]
    CHARSET = "utf-8"

    # The email body for recipient with non-HTML email clients.
    BODY_TEXT = "Hello,\r\nPlease find the attached file."

    # The HTML body of the email.
    BODY_HTML = """\
    <html>
    <head></head>
    <body>
    <p>This is an automated message sent by Where2Clean. This email contains cleaning data from the last 7 days attached in CSV format.</p>
    </body>
    </html>
    """

    client = boto3.client('ses', region_name=aws_region)
    log.info("recipient - {}".format(recipient))
    msg = MIMEMultipart('mixed')
    # Add subject, from and to lines.
    msg['Subject'] = subject 
    msg['From'] = sender 
    msg['To'] = recipient

    msg_body = MIMEMultipart('alternative')
    textpart = MIMEText(BODY_TEXT.encode(CHARSET), 'plain', CHARSET)
    htmlpart = MIMEText(BODY_HTML.encode(CHARSET), 'html', CHARSET)
    # Add the text and HTML parts to the child container.
    msg_body.attach(textpart)
    msg_body.attach(htmlpart)

    # Define the attachment part and encode it using MIMEApplication.
    att = MIMEApplication(open(file_name, 'rb').read())
    att.add_header('Content-Disposition','attachment', filename=csv_filename)

    if os.path.exists(file_name):
        log.info("Attachment file exists")
    else:
        log.info("Attachment file does not exists")

    # Attach the multipart/alternative child container to the multipart/mixed
    # parent container.
    msg.attach(msg_body)
    # Add the attachment to the parent container.
    msg.attach(att)

    try:
        #Provide the contents of the email.
        response = client.send_raw_email(
            Source=msg['From'],
            Destinations=[
                msg['To']
            ],
            RawMessage={
                'Data':msg.as_string(),
            }
        )
    # Display an error if something goes wrong. 
    except Exception as e:
        log.error(e)
    else:
        log.info("Email sent! Message ID: {}".format(response['MessageId']))
        return("Email sent! Message ID:", response['MessageId'])