import logging
import boto3
import json
import os
import time
import traceback
from datetime import datetime, timedelta
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)


endpoint_url = os.environ['ENDPOINT_URL'].strip()                       #https://48citdq98i.execute-api.ap-southeast-1.amazonaws.com/devW2C/
DYNAMODB_CONNECTIONS_TABLE =os.environ['CONNECTIONS_TABLE'].strip()     #diva-cleaning-connections
dynamodb_client = boto3.client('dynamodb')
firehose_delivery_stream =  os.environ['FIREHOSE_STREAM'].strip()       #diva-cleaning-facilities-delivery-stream'
api_client = boto3.client("apigatewaymanagementapi", endpoint_url=endpoint_url)

def lambda_handler(event, context):
    
    latest_qr_res=list()
    response_qr = {}
    response_dict = {"facilities": []}
    
    log.info(event)
    
    
  
    if (event['Records'][0]['eventName']) == "REMOVE":
        if len(event['Records']) == 2:
            #print(len(event['Records']))
            items = (event['Records'][1])
        else:
            return False
        items = (event['Records'][1])
    else:
        items = (event['Records'][0])

    if (event['Records'][0]['eventName']) == "REMOVE" and (event['Records'][1]['eventName']) == "REMOVE":
        return False
        
    print(items['dynamodb']['NewImage'])
    
    if 'pax_count_when_cleaned' in (items['dynamodb']['NewImage']):
        pax_count_when_cleaned = items['dynamodb']['NewImage']['pax_count_when_cleaned']['N']
    else:
        pax_count_when_cleaned = ""
            
    if 'last_cleaned_time' in (items['dynamodb']['NewImage']):
       last_cleaned_time = items['dynamodb']['NewImage']['last_cleaned_time']['S']
    else:
        last_cleaned_time = ""
                
    if 'assigned_callsign' in (items['dynamodb']['NewImage']):
        assigned_callsign = items['dynamodb']['NewImage']['assigned_callsign']['S']
    else:
        assigned_callsign = ""
            
    if 'ttl' in (items['dynamodb']['NewImage']):
        ttl = items['dynamodb']['NewImage']['ttl']['N']
    else:
        ttl = ""
    
    if 'last_cleaned_start' in (items['dynamodb']['NewImage']):
       last_cleaned_start = items['dynamodb']['NewImage']['last_cleaned_start']['S']
    else:
        last_cleaned_start = ""
            
    response_qr={
                'facility_code':items['dynamodb']['NewImage']['facility_code']['S'],
                'latest_cleaning_status':items['dynamodb']['NewImage']['latest_cleaning_status']['S'],
                'fault_type': items['dynamodb']['NewImage']['fault_type']['S'],
                'job_trigger_time':items['dynamodb']['NewImage']['job_trigger_time']['S'],
                'target_cleaned_time':items['dynamodb']['NewImage']['target_cleaned_time']['S'],
                'pax_count_when_cleaned':pax_count_when_cleaned,
                'last_cleaned_time':last_cleaned_time,
                'facility_name':items['dynamodb']['NewImage']['facility_name']['S'],
                'facility_type':items['dynamodb']['NewImage']['facility_type']['S'],
                'terminal':items['dynamodb']['NewImage']['terminal']['S'],
                'level':items['dynamodb']['NewImage']['level']['S'],
                'public_transit':items['dynamodb']['NewImage']['public_transit']['S'],
                'toilet_type': items['dynamodb']['NewImage']['toilet_type']['S'],
                "assigned_callsign" : assigned_callsign,
                "last_cleaned_start": last_cleaned_start,
                "ttl": ttl
                    
            }
        
    latest_qr_res.append(response_qr)
    
    for x in latest_qr_res:
        response_dict["facilities"].append(x.copy())
        

    response = {'statusCode': 200,'body': json.dumps({'message_type': 'OnFacilityUpdate','data': response_dict})}
            
    for p in response_dict['facilities']:
        m = (json.dumps(p)+"\n")
    #print(m)
    rec = m.encode("utf-8")
    #print(rec)
    try:
        record = m.encode("utf-8")
        sendBatchToFirehose(record, firehose_delivery_stream)
        print(record)
        log.info(response)
            
            
        try:
         # send all
            response = dynamodb_client.scan(TableName=DYNAMODB_CONNECTIONS_TABLE,)
        except Exception as e:
            log.error(e)
            raise ValueError(e)
        log.info("Connections db response: {}".format(response))
    
        message = json.dumps({'message_type': 'OnFacilityUpdate', 'data': response_dict})
        
        ## send all messages to all clients
        for item in response['Items']:
            connection_id = item['connection_id']['S']
            try:
                api_client.post_to_connection(
                Data=message,
                ConnectionId=connection_id
            )
            except api_client.exceptions.GoneException as e:
                log.error("Client {} returned 410 GONE, removing from connections DB".format(connection_id))
                
                try:
                    dynamodb_client.delete_item(
                    TableName = DYNAMODB_CONNECTIONS_TABLE,
                    Key = {"connection_id": {"S": connection_id}}
                )
                except Exception as e:
                    log.error("Error in deleting stale connection fron dynamodb: {}, connection_id: {}".format(e, connection_id))
        
            except Exception as e: # handle other errors
            # can handle GONE clients with api_client.exceptions.GoneException
                log.exception(e)
            else:
                log.info("Sent message {} to connection_id {}".format(
                json.dumps(message),
                connection_id
            ))
            
            
        return response
        
    except Exception as e:
        log.error(e)
        return {'statusCode': 500, 'body': json.dumps('Internal Server Error')}
        
     
    
def sendBatchToFirehose(blob, firehose_delivery_stream):

    firehose = boto3.client('firehose')
    try:
        response = firehose.put_record_batch(
            DeliveryStreamName=firehose_delivery_stream,
            Records=[
                {
                    'Data': blob
                },
            ]
        )
        
    except Exception as e:
        log.error("Event logging faced error: {}".format(e))
        return ("ERROR")
    else:
        log.info("Event logged without error: {}".format(response))
        return True
        
    