# snippet-comment:[These are tags for the AWS doc team's sample catalog. Do not remove.]
# snippet-sourcedescription:[websocket_connect.py implements a WebSocket $connect AWS Lambda function.]
# snippet-service:[lambda]
# snippet-keyword:[AWS Lambda]
# snippet-keyword:[Python]
# snippet-sourcesyntax:[python]
# snippet-sourcesyntax:[python]
# snippet-keyword:[Code Sample]
# snippet-sourcetype:[snippet]
# snippet-sourcedate:[2019-07-11]
# snippet-sourceauthor:[AWS]

# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
# http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.

import json
import logging
import os
import boto3
import datetime
from botocore.exceptions import ClientError

# Set up logging
log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

endpoint_url = os.environ["ENDPOINT_URL"]


def lambda_handler(event, context):
    """Example WebSocket $connect Lambda function

    :param event: Dict (usually) of parameters passed to the function
    :param context: LambdaContext object of runtime data
    :return: Dict of key:value pairs
    """

    # Log the values received in the event and context arguments
    log.info('$connect event: ' + json.dumps(event, indent=2))
    log.info(f'$connect event["requestContext"]["connectionId"]: {event["requestContext"]["connectionId"]}')

    # Retrieve the name of the DynamoDB table to store connection IDs
    table_name = os.environ['DYNAMODB_CONNECTIONS_TABLE']
     
    # # construct item to add to connections db
    # calc TTL and get connection ID 
    #TODO make env var in min
    ttl = int((datetime.datetime.today()+datetime.timedelta(days=1)).timestamp())
        # int dtype              today  + 1 day               in epoch time
    ttl = int(datetime.datetime.today().timestamp() + int(os.environ["TTL_IN_SEC"]))
    user_id = event['queryStringParameters']['user_id']
    connection_id = event['requestContext']['connectionId']
    item = {
        "connection_id": {"S": connection_id},
        "user_id": {"S": user_id},
        "ttl": {"N": str(ttl)}
    }

    log.info("Logging connection item: {}".format(item))
    
    # scan and filter out other people with same cognito id, notify them and remove them before putting new item
    # no longer doing this as front end does checks
    
    # # handle drivers connecting to 2 gates
    dynamodb_client = boto3.client("dynamodb")
       
    try:
        dynamodb_client.put_item(TableName=table_name, Item=item)
    except ClientError as e:
        log.error("Error putting item in table. Error: {}, item: {}".format(e, item))
        response = {'statusCode': 500, "body": json.dumps({"message": "Error putting item in table"})}
        # raise ConnectionAbortedError(e)
        return response
        
    # Construct response
    response = {'statusCode': 200, "body": json.dumps({"status": 200})}

    return response