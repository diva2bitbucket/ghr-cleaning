import logging
import boto3
import json
import os

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

endpoint_url = os.environ["ENDPOINT_URL"]

def lambda_handler(event, context):
    log.info('event: ' + json.dumps(event, indent=2))
    api_client = boto3.client("apigatewaymanagementapi", endpoint_url=endpoint_url)
    firehose_delivery_stream = os.environ['FIREHOSE_DELIVERY_STREAM']                           # diva-cleaning-gates-delivery-stream
    # get clients to send to by websocket    
    connections_table = os.environ["DYNAMODB_CONNECTIONS_TABLE"]
    dynamodb_client = boto3.client('dynamodb')
    
    # Compact batches - remove redundant batches or batches with deleteRecords
    event_records = compact_event(event)
    log.info('event_records: {}'.format(event_records))
    if event_records == []:
        log.info("Zero event records, aborting")
        return

    try:
        for record in event_records:
            record = json.dumps(record['new_image']).encode("utf-8")
            sendBatchToFirehose(record, firehose_delivery_stream)
    except Exception as e:
        log.error("Error while sending to firehose, logging details - records: {}, record: {}, exception: {}".format(event_records, record, e))
    
    # Retrieve all connection IDs from the table
    try:
        # send all
        response = dynamodb_client.scan(TableName=connections_table,)
    except Exception as e:
        log.error(e)
        raise ValueError(e)
    log.info("Connections db response: {}".format(response))
    
    message = json.dumps({'message_type': 'onGateUpdate', 'data': event_records})

    ## send all messages to all clients
    for item in response['Items']:
        connection_id = item['connection_id']['S']
        try:
            api_client.post_to_connection(
                Data=message,
                ConnectionId=connection_id
            )
        except api_client.exceptions.GoneException as e:
            log.error("Client {} returned 410 GONE, removing from connections DB".format(connection_id))
            try:
                dynamodb_client.delete_item(
                    TableName = connections_table,
                    Key = {"connection_id": {"S": connection_id}}
                )
            except Exception as e:
                log.error("Error in deleting stale connection fron dynamodb: {}, connection_id: {}".format(e, connection_id))
        except Exception as e: # handle other errors
            # can handle GONE clients with api_client.exceptions.GoneException
            log.exception(e)
        else:
            log.info("Sent message {} to connection_id {}".format(
                json.dumps(message),
                connection_id
            ))


def compact_event(event):
    """
    Compacts events - removes all deleted records
    :event dict: event object received by lambda
    :return event_records: compacted events to pass to front end
    """
    not_delete_events = list(filter(lambda x: x["eventName"] != "REMOVE", event["Records"]))
    
    event_records = sorted(
        not_delete_events, 
        key = lambda x: x["dynamodb"]["NewImage"]["display_datetime"]["S"]
    )
    
    new_images = [record["dynamodb"]["NewImage"] for record in event_records]
    event_types = [record["eventName"] for record in event_records]
    n_messages = len(new_images)
    n_gate_number = len(set([new_image["current_gate"]["S"] for new_image in new_images]))

    if (n_messages != n_gate_number):
        # something happens at a gate within the span of the last minute
        # take the last item happening at that gate by reversing and taking the first item
        temp = []
        seen_before = []
        for image, event_type in zip(new_images[::-1], event_types[::-1]):
            if image["current_gate"]["S"] not in seen_before:
                seen_before.append(image["current_gate"]["S"])
                temp.append(
                    {"NewImage": image,
                    "eventName": event_type}
                )
        new_images = [i["NewImage"] for i in temp]
        event_types = [i["eventName"] for i in temp]
    
    events = list(zip(new_images, event_types))
    events_list = []
    for e in events:
        events_list.append({
            "new_image": e[0],
            "event_type": e[1]
        })
    event_records = sorted(
        events_list, 
        key = lambda x: x["new_image"]["display_datetime"]["S"]
    )
    # return compacted_events
    compacted_events = []
    for event_record in event_records:
        for field in event_record["new_image"]:
            if "S" in event_record["new_image"][field]:
                event_record["new_image"][field] = event_record["new_image"][field]["S"]
            elif "N" in event_record["new_image"][field]:
                event_record["new_image"][field] = int(event_record["new_image"][field]["N"])
            elif "NULL" in event_record["new_image"][field]:
                event_record["new_image"][field] = ""
            elif "BOOL" in event_record["new_image"][field]:
                event_record["new_image"][field] = event_record["new_image"][field]["BOOL"]
        compacted_events.append(event_record)

    return compacted_events


def sendBatchToFirehose(blob, firehose_delivery_stream):
    """
    Wrapper to dump things in firehose
    :blob dict: json from dynamodb to dump in s3 via firehose
    :firehose_delivery_stream string: name of the kinesis data firehose
    :return bool: bool describing status of api call - successful=True
    """
    firehose = boto3.client('firehose')
    try:
        response = firehose.put_record_batch(
            DeliveryStreamName=firehose_delivery_stream,
            Records=[
                {
                    'Data': blob
                },
            ]
        )
    except Exception as e:
        log.error("Event logging faced error: {}".format(e))
        return False
    else:
        log.info("Event logged without error: {}".format(response))
        return True