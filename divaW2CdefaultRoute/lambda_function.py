import json
import logging
import traceback

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)


def lambda_handler(event, context):
    logging.info(event)

    try:
        temp = {
            'statusCode': 200,
            'body': json.dumps({
                'message_type': 'defaultRoute',
                'data': 'Invalid action. There are no backend actions with that name that can process your request.'
            })
        }
    except Exception as e:
        log.error('%s' %(e))
        log.error(traceback.format_exc())

    log.info(temp)

    return temp