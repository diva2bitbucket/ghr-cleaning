import os
import re
import glob
import json
import gzip
import boto3
import logging
import pandas as pd
import numpy as np
from subprocess import call
from datetime import datetime, timedelta, date

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

athena_client = boto3.client('athena')
s3_client = boto3.client('s3')
s3_resource = boto3.resource('s3')

DOWNLOAD_LOCATION_PATH = "/tmp/"


class ExportRecords():
    
    def __init__(self):

        self.source_s3_bucket_name = os.environ["SOURCE_S3_BUCKET_NAME"]                # diva-prod-cleaning-logs
        self.mapping_files_source_bucket = os.environ['MAPPING_FILES_SOURCE_BUCKET']    # diva-prod-cleaning-mapping-files
        self.terminal_gates_json_file = os.environ['TERMINAL_GATES_JSON_FILE']          # terminal_gates.json
        self.max_record_count = int(os.environ['MAX_RECORD_COUNT'])                     # ex. 1000

        self.query_status_json = {'query_status': {'status': '', 'source': 'Where2Clean Export Records', 'code': '200'}}

        self.now_timestamp = datetime.now() + timedelta(hours=8)
        self.now_hours_minutes = datetime.strftime(self.now_timestamp, "%H:%M")
        self.date_today = date.today()

        #log.info("now_timestamp - %s" % self.now_timestamp)

        object_content = s3_resource.Object(self.mapping_files_source_bucket, self.terminal_gates_json_file)
        file_content = object_content.get()['Body'].read().decode('utf-8')
        self.terminal_gates = json.loads(file_content)

        log.info("terminal_gates - %s" % self.terminal_gates)

        self.keys_list = []
        self.list_of_files = []
        self.gates_list = []
        self.start_date = ""
        self.search_prefix = "flights"


    def get_gates_list(self):

        if self.terminal == '1':
            self.gates_list = self.terminal_gates["Terminal 1"]
        if self.terminal == '2':
            self.gates_list = self.terminal_gates["Terminal 2"]
        if self.terminal == '3':
            self.gates_list = self.terminal_gates["Terminal 3"]
        if self.terminal == '4':
            self.gates_list = self.terminal_gates["Terminal 4"]


    def local_directory(self):
        '''
        1. Create a local directory inside Lambda to store the ZIP files temporarily 
        2. Usually temporary files are stored in "/tmp" location within Lambda 
        '''
            
        # Temporary use of non-persistent disk space in Lambda's own '/tmp' directory for downloading files from source S3

        if not os.path.exists(DOWNLOAD_LOCATION_PATH):
            log.info("Creating local directory /tmp to store the files")
            os.mkdir(DOWNLOAD_LOCATION_PATH)
        else:
            log.info('tmp directory already exists, clean it up before re-using')
            call('rm -rf /tmp/*', shell=True)


    def download_files_from_source_s3(self):
        # Create local directory inside Lambda if it does not exist 
        self.local_directory()
            
        key_str_list = []

        # Connect to S3 bucket
        source_s3_bucket = s3_resource.Bucket(self.source_s3_bucket_name)

        flights_ = "flights"
        search_year = self.search_start_datetime.strftime("%Y")
        search_month = self.search_start_datetime.strftime("%m")
        search_day = self.search_start_datetime.strftime("%d")
        search_hour = self.search_start_datetime.strftime("%H") + '/'

        search_start_after = os.path.join(flights_, search_year, search_month, search_day, search_hour)

        # Create a reusable Paginator to search for more than 1000 objects
        paginator = s3_client.get_paginator('list_objects_v2')

        # Create a PageIterator from the Paginator 
        page_iterator = paginator.paginate(Bucket=self.source_s3_bucket_name, Prefix=self.search_prefix, StartAfter=search_start_after) 
        
        for page in page_iterator:
            if 'Contents' in page:  # To check if the bucket has at least one object
                for obj in page['Contents']:
                    key_str = obj['Key']

                    # Look for keys that ends with .csv
                    if key_str.endswith(".gz"):

                        key_str_filename = str.split(str.split(key_str)[0], '/')[-1]

                        key_str_list = key_str.split("/")
                        # ex. flights/2020/08/19/14/diva-cleaning-flights-delivery-stream-2-2020-08-19-14-57-13-bbae6eaa-f3af-486b-b5d0-dc846845826d.gz
                        file_dt = key_str_list[1] + "-" + key_str_list[2] + "-" + key_str_list[3] + " " + key_str_list[4] + ":00:00"
                        file_date = datetime.strptime(file_dt, "%Y-%m-%d %H:%M:%S")

                        if file_date <= self.search_end_datetime:
                            csv_temp_path = DOWNLOAD_LOCATION_PATH + key_str_filename

                            # Download S3 object to a file
                            source_s3_bucket.meta.client.download_file(self.source_s3_bucket_name, key_str, csv_temp_path)
                            
                            # Append all the ZIP files to a list
                            self.keys_list.append(obj['Key'])
                            list_of_files = glob.glob(os.path.join(DOWNLOAD_LOCATION_PATH, "*"))
            else:
                # No objects found
                log.info('No files found in source S3 bucket')
        
        return list_of_files


    def export_records(self):
        
        response_json = None
        record_count = 0
        self.get_gates_list()

        flight_record = {
            'flight_id': '',
            'direction': '', 
            'flightno': '', 
            'current_gate': '', 
            'arr_dep_flag': '', 
            'unavailable_start': '', 
            'display_datetime': '', 
            'unavailable_end': '', 
            'scheduled_date': '', 
            'previous_gate': '', 
            'delete_marker': '', 
            'aircraft_type': '', 
            'message_timestamp': '',
            'user_id': '',
            'cleaning_status': '',
            'last_cleaned_start': '',
            'last_cleaned_complete': '',
            'cleaned_priority': 0,
            'hand_railings_good_condition': '',
            'ceiling_finishes_good_condition': '',
            'seats_good_condition': '',
            'floor_finishes_good_condition': '',
            'wall_finishes_good_condition': '',
            'air_con_good_condition': '',
            'bins_good_condition': '',
            'water_cooler_good_condition': '',
            'down_ramp_good_condition': '',
            'other_fixtures_good_condition': ''
        }
        ni_list = []

        list_of_gzip_files = self.download_files_from_source_s3()

        for gzip_file in list_of_gzip_files:

            gzipfile = open(gzip_file, 'rb')
            magic_header = gzipfile.read(2)
            if magic_header == b'\x1f\x8b':
                gzipfile.seek(0)
                archive = gzip.GzipFile(fileobj=gzipfile)
                file_content = archive.read().decode("utf-8")
            else:
                gzipfile.seek(0)
                file_content = gzipfile.read().decode("utf-8")

            if len(file_content) > 0:
                ni = 0
                has_records = True
                while has_records:
                    fc = file_content[ni:]
                    bs = fc.find("{")
                    be = fc.find("}") + 1
                    n_i = json.loads(fc[bs:be])
                    n_i_keys = n_i.keys()

                    if self.gates_list != []:
                        if n_i['current_gate'] in self.gates_list:
                            flight_record['flight_id'] = n_i['flight_id']
                            flight_record['direction'] = n_i['direction'] 
                            flight_record['flightno'] = n_i['flightno']
                            flight_record['current_gate'] = n_i['current_gate']
                            flight_record['arr_dep_flag'] = n_i['arr_dep_flag']

                            if 'unavailable_start' in n_i_keys:
                                flight_record['unavailable_start'] = n_i['unavailable_start']

                            flight_record['display_datetime'] = n_i['display_datetime']

                            if 'unavailable_end' in n_i_keys:
                                flight_record['unavailable_end'] = n_i['unavailable_end']

                            flight_record['scheduled_date'] = n_i['scheduled_date']
                            flight_record['previous_gate'] = n_i['previous_gate']
                            flight_record['delete_marker'] = n_i['delete_marker']
                            flight_record['aircraft_type'] = n_i['aircraft_type']
                            flight_record['message_timestamp'] = n_i['message_timestamp']

                            if 'user_id' in n_i_keys:
                                flight_record['user_id'] = n_i['user_id']
                            if 'cleaning_status' in n_i_keys:
                                flight_record['cleaning_status'] = n_i['cleaning_status']
                            if 'last_cleaned_start' in n_i_keys:
                                flight_record['last_cleaned_start'] = n_i['last_cleaned_start']
                            if 'last_cleaned_complete' in n_i_keys:
                                flight_record['last_cleaned_complete'] = n_i['last_cleaned_complete']
                            if 'cleaned_priority' in n_i_keys:
                                flight_record['cleaned_priority'] = n_i['cleaned_priority']
                            if 'hand_railings_good_condition' in n_i_keys:
                                flight_record['hand_railings_good_condition'] = n_i['hand_railings_good_condition']
                            if 'ceiling_finishes_good_condition' in n_i_keys:
                                flight_record['ceiling_finishes_good_condition'] = n_i['ceiling_finishes_good_condition']
                            if 'seats_good_condition' in n_i_keys:
                                flight_record['seats_good_condition'] = n_i['seats_good_condition']
                            if 'floor_finishes_good_condition' in n_i_keys:
                                flight_record['floor_finishes_good_condition'] = n_i['floor_finishes_good_condition']
                            if 'wall_finishes_good_condition' in n_i_keys:
                                flight_record['wall_finishes_good_condition'] = n_i['wall_finishes_good_condition']
                            if 'air_con_good_condition' in n_i_keys:
                                flight_record['air_con_good_condition'] = n_i['air_con_good_condition']
                            if 'bins_good_condition' in n_i_keys:
                                flight_record['bins_good_condition'] = n_i['bins_good_condition']
                            if 'water_cooler_good_condition' in n_i_keys:
                                flight_record['water_cooler_good_condition'] = n_i['water_cooler_good_condition']
                            if 'down_ramp_good_condition' in n_i_keys:
                                flight_record['down_ramp_good_condition'] = n_i['down_ramp_good_condition']
                            if 'other_fixtures_good_condition' in n_i_keys:
                                flight_record['other_fixtures_good_condition'] = n_i['other_fixtures_good_condition']

                            ni_list.append(flight_record.copy())
                            record_count += 1
                    else:
                        flight_record['flight_id'] = n_i['flight_id']
                        flight_record['direction'] = n_i['direction'] 
                        flight_record['flightno'] = n_i['flightno']
                        flight_record['current_gate'] = n_i['current_gate']
                        flight_record['arr_dep_flag'] = n_i['arr_dep_flag']

                        if 'unavailable_start' in n_i_keys:
                            flight_record['unavailable_start'] = n_i['unavailable_start']

                        flight_record['display_datetime'] = n_i['display_datetime']

                        if 'unavailable_end' in n_i_keys:
                            flight_record['unavailable_end'] = n_i['unavailable_end']

                        flight_record['scheduled_date'] = n_i['scheduled_date']
                        flight_record['previous_gate'] = n_i['previous_gate']
                        flight_record['delete_marker'] = n_i['delete_marker']
                        flight_record['aircraft_type'] = n_i['aircraft_type']
                        flight_record['message_timestamp'] = n_i['message_timestamp']

                        if 'user_id' in n_i_keys:
                            flight_record['user_id'] = n_i['user_id']
                        if 'cleaning_status' in n_i_keys:
                            flight_record['cleaning_status'] = n_i['cleaning_status']
                        if 'last_cleaned_start' in n_i_keys:
                            flight_record['last_cleaned_start'] = n_i['last_cleaned_start']
                        if 'last_cleaned_complete' in n_i_keys:
                            flight_record['last_cleaned_complete'] = n_i['last_cleaned_complete']
                        if 'cleaned_priority' in n_i_keys:
                            flight_record['cleaned_priority'] = n_i['cleaned_priority']
                        if 'hand_railings_good_condition' in n_i_keys:
                            flight_record['hand_railings_good_condition'] = n_i['hand_railings_good_condition']
                        if 'ceiling_finishes_good_condition' in n_i_keys:
                            flight_record['ceiling_finishes_good_condition'] = n_i['ceiling_finishes_good_condition']
                        if 'seats_good_condition' in n_i_keys:
                            flight_record['seats_good_condition'] = n_i['seats_good_condition']
                        if 'floor_finishes_good_condition' in n_i_keys:
                            flight_record['floor_finishes_good_condition'] = n_i['floor_finishes_good_condition']
                        if 'wall_finishes_good_condition' in n_i_keys:
                            flight_record['wall_finishes_good_condition'] = n_i['wall_finishes_good_condition']
                        if 'air_con_good_condition' in n_i_keys:
                            flight_record['air_con_good_condition'] = n_i['air_con_good_condition']
                        if 'bins_good_condition' in n_i_keys:
                            flight_record['bins_good_condition'] = n_i['bins_good_condition']
                        if 'water_cooler_good_condition' in n_i_keys:
                            flight_record['water_cooler_good_condition'] = n_i['water_cooler_good_condition']
                        if 'down_ramp_good_condition' in n_i_keys:
                            flight_record['down_ramp_good_condition'] = n_i['down_ramp_good_condition']
                        if 'other_fixtures_good_condition' in n_i_keys:
                            flight_record['other_fixtures_good_condition'] = n_i['other_fixtures_good_condition']

                        ni_list.append(flight_record.copy())
                        record_count += 1

                    flight_record = {
                        'flight_id': '',
                        'direction': '', 
                        'flightno': '', 
                        'current_gate': '', 
                        'arr_dep_flag': '', 
                        'unavailable_start': '', 
                        'display_datetime': '', 
                        'unavailable_end': '', 
                        'scheduled_date': '', 
                        'previous_gate': '', 
                        'delete_marker': '', 
                        'aircraft_type': '', 
                        'message_timestamp': '',
                        'user_id': '',
                        'cleaning_status': '',
                        'last_cleaned_start': '',
                        'last_cleaned_complete': '',
                        'cleaned_priority': 0,
                        'hand_railings_good_condition': '',
                        'ceiling_finishes_good_condition': '',
                        'seats_good_condition': '',
                        'floor_finishes_good_condition': '',
                        'wall_finishes_good_condition': '',
                        'air_con_good_condition': '',
                        'bins_good_condition': '',
                        'water_cooler_good_condition': '',
                        'down_ramp_good_condition': '',
                        'other_fixtures_good_condition': ''
                    }

                    ni = file_content.find("{", ni+be)
                    if (ni == -1):
                        has_records = False

                if record_count >= self.max_record_count:
                    break

        if len(ni_list) > 0:
            ni_df = pd.DataFrame.from_records(ni_list, columns=['flight_id', 'direction', 'flightno', 'current_gate', 'arr_dep_flag', 'unavailable_start', 'display_datetime', 'unavailable_end', 'scheduled_date', 'previous_gate', 'delete_marker', 'aircraft_type', 'message_timestamp', 'user_id', 'cleaning_status', 'last_cleaned_start', 'last_cleaned_complete', 'cleaned_priority', 'hand_railings_good_condition', 'ceiling_finishes_good_condition', 'seats_good_condition', 'floor_finishes_good_condition', 'wall_finishes_good_condition', 'air_con_good_condition', 'bins_good_condition', 'water_cooler_good_condition', 'down_ramp_good_condition', 'other_fixtures_good_condition'])
            
            response_json = json.loads(ni_df.to_json(orient="records"))
        else:
            self.query_status_json['query_status']['status'] = "No records found for this query"
            response_json = self.query_status_json

        return response_json


    def process_request_parameters(self, request):

        request_json = json.loads(request)
        request_json = json.loads(request_json)

        response_json = None
        validation_errors = ""

        if request.find("terminal") >= 0:
            terminal = request_json['terminal']
            if terminal != '':
                terminal_regex_result = re.search(r'^([0-4]{1})|(all)|(All)|(ALL)$', terminal)
                if terminal_regex_result:
                    self.terminal = terminal
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid terminal value. Expected value should be one of the following: all, 1, 2, 3, 4."
                    else:
                        validation_errors += ", Invalid terminal value. Expected value should be one of the following: all, 1, 2, 3, 4."
            else:
                validation_errors = "Required parameter terminal is blank. Expected value should be one of the following: all, 1, 2, 3, 4."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter terminal."
            else:
                validation_errors += ", Missing required parameter terminal."

        if request.find("start_date") >= 0:
            start_date = request_json['start_date']
            if start_date != '':
                scheduledatetime_from_regex_result = re.search(r'^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9]{3}$', start_date)
                if scheduledatetime_from_regex_result:
                    self.start_date = start_date
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid start_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
                    else:
                        validation_errors += ", Invalid start_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
            else:
                validation_errors = "Required parameter start_date is blank."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter start_date."
            else:
                validation_errors += ", Missing required parameter start_date."

        if request.find("end_date") >= 0:
            end_date = request_json['end_date']
            if end_date != '':
                scheduledatetime_to_regex_result = re.search(r'^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9]{3}$', end_date)
                if scheduledatetime_to_regex_result:
                    self.end_date = end_date
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid end_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
                    else:
                        validation_errors += ", Invalid end_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
            else:
                validation_errors = "Required parameter end_date is blank."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter end_date."
            else:
                validation_errors += ", Missing required parameter end_date."

        if validation_errors != "":
            response_json = {"error": {"status": "failed", "source": "Where2Clean Export Records", "message": validation_errors, "code": 400}}
        else:
            sd = self.start_date.split("T")[0]
            ed = self.end_date.split("T")[0]
            sdt = datetime.strptime(sd, '%Y-%m-%d').date()
            edt = datetime.strptime(ed, '%Y-%m-%d').date()

            if sdt > edt:
                if validation_errors == "":
                    validation_errors = "start_date parameter cannot be greater than the end_date parameter."
                else:
                    validation_errors += ", start_date parameter cannot be greater than the end_date parameter."
                    
                response_json = {"error": {"status": "failed", "source": "Where2Clean Export Records", "message": validation_errors, "code": 400}}
            else:
                ssd = self.start_date.split("T")[0] + " " + self.start_date.split("T")[1].split(".")[0]
                self.search_start_datetime = datetime.strptime(ssd, "%Y-%m-%d %H:%M:%S")
                log.info("search_start_datetime - %s" % self.search_start_datetime)
                sed = self.end_date.split("T")[0] + " " + self.end_date.split("T")[1].split(".")[0]
                self.search_end_datetime = datetime.strptime(sed, "%Y-%m-%d %H:%M:%S")
                log.info("search_end_datetime - %s" % self.search_end_datetime)

        return response_json


    # MAIN METHOD
    def main(self, request):

        response_json = self.process_request_parameters(request)

        if response_json is None:

            response_json = self.export_records()

        return response_json


def lambda_handler(event, context):

    main_class = ExportRecords()
    print("LOG: event - ", event)
    if event['body'] != None:            #and event['queryStringParameters'] != None:
        request = json.dumps(event['body'])
        #query_param = json.dumps(event['queryStringParameters'])
        response_json = main_class.main(request)
        print("LOG: response_json - ", response_json)
    else:
        response_json = {"status" : "Missing request parameters", "status_code" : "400"}

    return {"statusCode": 200, "headers": {"Content-Type": "application/json"}, "body": json.dumps(response_json), "isBase64Encoded": False}