# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import re
import boto3
import json
import logging
from datetime import datetime, timedelta, date

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

athena_client = boto3.client('athena')
s3_resource = boto3.resource('s3')


class ExportRecords():
    
    def __init__(self):

        self.output_location = os.environ["OUTPUT_LOCATION"]                            # s3://diva-prod-w2c-athena-query-results/
        self.mapping_files_source_bucket = os.environ['MAPPING_FILES_SOURCE_BUCKET']    # diva-prod-cleaning-mapping-files
        self.terminal_gates_json_file = os.environ['TERMINAL_GATES_JSON_FILE']          # terminal_gates.json

        self.query_status_json = {'status': '', 'message': '', 'records': [], 'code': '200'}

        self.now_timestamp = datetime.now() + timedelta(hours=8)
        self.now_hours_minutes = datetime.strftime(self.now_timestamp, "%H:%M")
        self.date_today = date.today()

        self.from_interval = ""
        self.to_interval = ""
        self.from_clause = ""
        self.to_clause = ""
        self.gates_list = []

        log.info("now_timestamp - %s" % self.now_timestamp)

        object_content = s3_resource.Object(self.mapping_files_source_bucket, self.terminal_gates_json_file)
        file_content = object_content.get()['Body'].read().decode('utf-8')
        self.terminal_gates = json.loads(file_content)

        log.info("terminal_gates - %s" % self.terminal_gates)


    def set_terminal_clause_value(self):

        if self.terminal == '1':
            self.gates_list = self.terminal_gates["Terminal 1"]
        if self.terminal == '2':
            self.gates_list = self.terminal_gates["Terminal 2"]
        if self.terminal == '3':
            self.gates_list = self.terminal_gates["Terminal 3"]
        if self.terminal == '4':
            self.gates_list = self.terminal_gates["Terminal 4"]


    def set_from_clause_value(self):

        dl = self.start_date.split("T")
        td = dl[0]
        dt = datetime.strptime(td, '%Y-%m-%d').date()

        if dt < self.date_today:
            self.from_interval = str(self.date_today - dt).split(" ")[0]
            self.from_clause = "current_date - interval '" + self.from_interval + "' day"
        else:
            self.from_clause = "current_date"


    def set_to_clause_value(self):

        dl = self.end_date.split("T")
        td = dl[0]
        dt = datetime.strptime(td, '%Y-%m-%d').date()

        if dt < self.date_today:
            self.to_interval = str(self.date_today - dt).split(" ")[0]
            self.to_clause = "current_date - interval '" + self.to_interval + "' day"
        else:
            self.to_clause = "current_date"


    def export_records(self):

        self.set_terminal_clause_value()
        self.set_from_clause_value()
        self.set_to_clause_value()

        response1 = None
        response2 = None
        response3 = None
        response_json = None

        resultlist = []
        resultset = {}
        query_status = ""

        if self.terminal.lower() != "all":

            gates_list_ = str(self.gates_list)[1:-1]        # remove the trailing square brackets ("[", "]")

            query_string = "SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, cleaned_priority, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition FROM (SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, CAST(display_datetime AS TIMESTAMP) AS display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, cleaned_priority, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition from flights) WHERE display_datetime BETWEEN from_iso8601_timestamp(CONCAT(to_iso8601(" + self.from_clause + "),'T00:00:00.000Z')) AND from_iso8601_timestamp(CONCAT(to_iso8601(" + self.to_clause + "),'T23:59:59.999Z')) AND current_gate = ANY (VALUES " + gates_list_ + ") ORDER BY display_datetime LIMIT 1000;"
        else:
            query_string = "SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, cleaned_priority, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition FROM (SELECT flight_id, direction, flightno, current_gate, arr_dep_flag, unavailable_start, CAST(display_datetime AS TIMESTAMP) AS display_datetime, unavailable_end, scheduled_date, previous_gate, delete_marker, aircraft_type, message_timestamp, user_id, cleaning_status, cleaned_priority, last_cleaned_start, last_cleaned_complete, hand_railings_good_condition, ceiling_finishes_good_condition, seats_good_condition, floor_finishes_good_condition, wall_finishes_good_condition, air_con_good_condition, bins_good_condition, water_cooler_good_condition, down_ramp_good_condition, other_fixtures_good_condition from flights) WHERE display_datetime BETWEEN from_iso8601_timestamp(CONCAT(to_iso8601(" + self.from_clause + "),'T00:00:00.000Z')) AND from_iso8601_timestamp(CONCAT(to_iso8601(" + self.to_clause + "),'T23:59:59.999Z')) ORDER BY display_datetime LIMIT 1000;"

        log.info("query_string - %s" % query_string)

        try:
            response1 = athena_client.start_query_execution(
                QueryString=query_string,
                QueryExecutionContext={
                    'Database': 'where2clean',
                    'Catalog': 'AwsDataCatalog'
                },
                ResultConfiguration={
                    'OutputLocation': self.output_location,
                    'EncryptionConfiguration': {
                        'EncryptionOption': 'SSE_S3'
                    }
                },
                WorkGroup='primary'
            )

            if response1 != None and response1 != "":
                log.info("response1 - %s" % response1)
                query_execution_id = response1['QueryExecutionId']

                while query_status != "SUCCEEDED":
                    response2 = athena_client.get_query_execution(
                        QueryExecutionId=query_execution_id
                    )
                    query_status = response2['QueryExecution']['Status']['State']
                else:
                    log.info("query_status - %s" % query_status)

                    response3 = athena_client.get_query_results(
                        QueryExecutionId=query_execution_id,
                        MaxResults=1000
                    )
                    if response3 != None and response3 != "":
                        #log.info("response3 - %s" % response3['ResultSet']['Rows'])

                        rows = response3['ResultSet']['Rows']

                        for i in rows[0]['Data']:               # first row is the HEADER, get the column names
                            resultset[i['VarCharValue']] = ""
                        #log.info("resultset - %s" % resultset)
                        x = 1                                   # second row and onwards
                        y = len(rows)                           # number of returned data including the header
                        rl = list(resultset)                    # list version of the dict resultset

                        while x < y:                            # loop over the returned data starting at the secord row (i.e. not including the header)
                            for i in range(len(resultset)):     # for each item in the header
                                # get the value from rows[x] and assign the value to the header item
                                # example: for "flight_id" header item the value is "ARR-SQ25-2021-02-05"
                                # otherwise, if rows[x] is empty (i.e. {}), assign a blank value ("")
                                resultset[rl[i]] = rows[x]['Data'][i]['VarCharValue'] if rows[x]['Data'][i] != {} else ""

                            # store resultset to resultlist
                            resultlist.append(resultset.copy())
                            x += 1

        except Exception as e:
            log.error(e)

        if resultlist == []:
            self.query_status_json['status'] = "success"
            self.query_status_json['message'] = "No records found for this query"
        else:
            if y == 1000:
                self.query_status_json['status'] = "truncated"
                self.query_status_json['message'] = "Due to large number of records, it has been truncated to 1,000 max limit. "
            else:
                self.query_status_json['status'] = "success"
            self.query_status_json['records'] = resultlist

        response_json = self.query_status_json

        return response_json


    def process_request_parameters(self, request):

        request_json = json.loads(request)
        request_json = json.loads(request_json)

        response_json = None
        validation_errors = ""

        if request.find("terminal") >= 0:
            terminal = request_json['terminal']
            if terminal != '':
                terminal_regex_result = re.search(r'^([0-4]{1})|(all)|(All)|(ALL)$', terminal)
                if terminal_regex_result:
                    self.terminal = terminal
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid terminal value. Expected value should be one of the following: all, 1, 2, 3, 4."
                    else:
                        validation_errors += ", Invalid terminal value. Expected value should be one of the following: all, 1, 2, 3, 4."
            else:
                validation_errors = "Required parameter terminal is blank. Expected value should be one of the following: all, 1, 2, 3, 4."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter terminal."
            else:
                validation_errors += ", Missing required parameter terminal."

        if request.find("start_date") >= 0:
            start_date = request_json['start_date']
            if start_date != '':
                scheduledatetime_from_regex_result = re.search(r'^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9]{3}$', start_date)
                if scheduledatetime_from_regex_result:
                    self.start_date = start_date
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid start_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
                    else:
                        validation_errors += ", Invalid start_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
            else:
                validation_errors = "Required parameter start_date is blank."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter start_date."
            else:
                validation_errors += ", Missing required parameter start_date."

        if request.find("end_date") >= 0:
            end_date = request_json['end_date']
            if end_date != '':
                scheduledatetime_to_regex_result = re.search(r'^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9]{3}$', end_date)
                if scheduledatetime_to_regex_result:
                    self.end_date = end_date
                else:
                    if validation_errors == "":
                        validation_errors = "Invalid end_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
                    else:
                        validation_errors += ", Invalid end_date value. Required format is yyyy-MM-ddThh:mm:ss.zzz"
            else:
                validation_errors = "Required parameter end_date is blank."
        else:
            if validation_errors == "":
                validation_errors = "Missing required parameter end_date."
            else:
                validation_errors += ", Missing required parameter end_date."

        if validation_errors != "":
            response_json = {"status": "error", "message": validation_errors, "code": 400}
        else:
            sd = self.start_date.split("T")[0]
            ed = self.end_date.split("T")[0]
            sdt = datetime.strptime(sd, '%Y-%m-%d').date()
            edt = datetime.strptime(ed, '%Y-%m-%d').date()

            if sdt > edt:
                if validation_errors == "":
                    validation_errors = "start_date parameter cannot be greater than the end_date parameter."
                else:
                    validation_errors += ", start_date parameter cannot be greater than the end_date parameter."
                    
                response_json = {"status": "failed", "message": validation_errors, "code": 400}

        return response_json


    # MAIN METHOD
    def main(self, request):

        response_json = self.process_request_parameters(request)

        if response_json is None:

            response_json = self.export_records()

        return response_json


def lambda_handler(event, context):

    main_class = ExportRecords()
    print("LOG: event - ", event)
    if event['body'] != None:            #and event['queryStringParameters'] != None:
        request = json.dumps(event['body'])
        #query_param = json.dumps(event['queryStringParameters'])
        response_json = main_class.main(request)
        print("LOG: response_json - ", response_json)
    else:
        response_json = {"query_status": {"status": "failed", "source": "Where2Clean Export Records", "message": "Missing request parameters", "code": 400}}

    return {"statusCode": 200, "headers": {"Content-Type": "application/json"}, "body": json.dumps(response_json), "isBase64Encoded": False}