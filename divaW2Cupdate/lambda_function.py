import os
import re
import json
import boto3
import logging

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

s3_resource = boto3.resource('s3')

mapping_files_source_bucket = os.environ['MAPPING_FILES_SOURCE_BUCKET']    # diva-dev-cleaning-mapping-files
terminal_gates_json_file = os.environ['TERMINAL_GATES_JSON_FILE']          # terminal_gates.json

object_content = s3_resource.Object(mapping_files_source_bucket, terminal_gates_json_file)
file_content = object_content.get()['Body'].read().decode('utf-8')
terminal_gates = json.loads(file_content)

terminal_1_gates = terminal_gates['Terminal 1']
terminal_2_gates = terminal_gates['Terminal 2']
terminal_3_gates = terminal_gates['Terminal 3']
terminal_4_gates = terminal_gates['Terminal 4']


def lambda_handler(event, context):
    # verify user performing event request
    # TODO
    log.info("event - {}".format(json.dumps(event)))
    req_body = json.loads(event["body"])
    req_body_keys = req_body.keys()

    user_id_ = ""
    ttl = 0
    cleaned_priority = 0
    response = {}

    access_token = req_body["access_token"]
    flight_id = req_body["flight_id"]
    #user_id = req_body["user_id"]
    current_gate_ = req_body["current_gate"].upper()
    cleaning_status = req_body["cleaning_status"].upper()
    last_cleaned_start = req_body["last_cleaned_start"]
    last_cleaned_complete = req_body["last_cleaned_complete"]
    ceiling_finishes_good_condition = req_body["ceiling_finishes_good_condition"]
    wall_finishes_good_condition = req_body["wall_finishes_good_condition"]
    floor_finishes_good_condition = req_body["floor_finishes_good_condition"]
    water_cooler_good_condition = req_body["water_cooler_good_condition"]
    seats_good_condition = req_body["seats_good_condition"]
    bins_good_condition = req_body["bins_good_condition"]
    down_ramp_good_condition = req_body["down_ramp_good_condition"]
    other_fixtures_good_condition = req_body["other_fixtures_good_condition"]
    hand_railings_good_condition = req_body["hand_railings_good_condition"]
    air_con_good_condition = req_body["air_con_good_condition"]
    high_touch_points_good_condition = req_body["high_touch_points_good_condition"]
    counters_good_condition = req_body["counters_good_condition"]
 
    cognito_idp = boto3.client("cognito-idp")
    try:
        cog_response = cognito_idp.get_user(AccessToken=access_token)
    except cognito_idp.exceptions.NotAuthorizedException as err:
        response = {'statusCode': 401, 'body': json.dumps({'ERROR': err})}
        return response # to stop further processing
    except Exception as e:
        log.error("Unknown error while checking auth: {}".format(str(e)))
    else:
        log.info("Cognito call successful, user is authorised; response: {}".format(cog_response))
        user_id_ = cog_response['Username']
        log.info("user_id_ - {}".format(user_id_))

    validation_errors = ""

    if cleaning_status != "":
        status_regex = re.search('^(CLEANING)|(COMPLETE)$', cleaning_status)
        if not status_regex:
            if validation_errors != "":
                validation_errors += ", invalid Cleaning Status value"
            else:
                validation_errors += "invalid Cleaning Status value"

    start_regex = re.search('^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$', last_cleaned_start)
    if not start_regex:
        if validation_errors != "":
            validation_errors += ", invalid Last Cleaned Start Time value"
        else:
            validation_errors += "invalid Last Cleaned Start Time value"

    if cleaning_status == "COMPLETE":
        complete_regex = re.search('^[1-2][0-9][0-9]{2}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$', last_cleaned_complete)
        if not complete_regex:
            if validation_errors != "":
                validation_errors += ", invalid Last Cleaned Complete Time value"
            else:
                validation_errors += "invalid Last Cleaned Complete Time value"
        if "priority" in req_body_keys:
            cleaned_priority = int(req_body["priority"])
        else:
            if validation_errors != "":
                validation_errors += ", missing priority field value"
            else:
                validation_errors += "missing priority field value"

    if ceiling_finishes_good_condition != "":
        ceiling_finishes_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', ceiling_finishes_good_condition)
        if not ceiling_finishes_regex:
            if validation_errors != "":
                validation_errors += ", invalid Ceiling Finishes Good Condition value"
            else:
                validation_errors += "invalid Ceiling Finishes Good Condition value"
        else:
            if ceiling_finishes_good_condition.upper() == "TRUE":
                ceiling_finishes_good_condition = "TRUE"
            elif ceiling_finishes_good_condition.upper() == "FALSE":
                ceiling_finishes_good_condition = "FALSE"

    if wall_finishes_good_condition != "":
        wall_finishes_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', wall_finishes_good_condition)
        if not wall_finishes_regex:
            if validation_errors != "":
                validation_errors += ", invalid Wall Finishes Good Condition value"
            else:
                validation_errors += "invalid Wall Finishes Good Condition value"
        else:
            if wall_finishes_good_condition.upper() == "TRUE":
                wall_finishes_good_condition = "TRUE"
            elif wall_finishes_good_condition.upper() == "FALSE":
                wall_finishes_good_condition = "FALSE"

    if floor_finishes_good_condition != "":
        floor_finishes_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', floor_finishes_good_condition)
        if not floor_finishes_regex:
            if validation_errors != "":
                validation_errors += ", invalid Floor Finishes Good Condition value"
            else:
                validation_errors += "invalid Floor Finishes Good Condition value"
        else:
            if floor_finishes_good_condition.upper() == "TRUE":
                floor_finishes_good_condition = "TRUE"
            elif floor_finishes_good_condition.upper() == "FALSE":
                floor_finishes_good_condition = "FALSE"

    if water_cooler_good_condition != "":
        water_cooler_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', water_cooler_good_condition)
        if not water_cooler_regex:
            if validation_errors != "":
                validation_errors += ", invalid Water Cooler Good Condition value"
            else:
                validation_errors += "invalid Water Cooler Good Condition value"
        else:
            if water_cooler_good_condition.upper() == "TRUE":
                water_cooler_good_condition = "TRUE"
            elif water_cooler_good_condition.upper() == "FALSE":
                water_cooler_good_condition = "FALSE"

    if seats_good_condition != "":
        seats_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', seats_good_condition)
        if not seats_regex:
            if validation_errors != "":
                validation_errors += ", invalid Seats Good Condition value"
            else:
                validation_errors += "invalid Seats Good Condition value"
        else:
            if seats_good_condition.upper() == "TRUE":
                seats_good_condition = "TRUE"
            elif seats_good_condition.upper() == "FALSE":
                seats_good_condition = "FALSE"

    if bins_good_condition != "":
        bins_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', bins_good_condition)
        if not bins_regex:
            if validation_errors != "":
                validation_errors += ", invalid Bins Good Condition value"
            else:
                validation_errors += "invalid Bins Good Condition value"
        else:
            if bins_good_condition.upper() == "TRUE":
                bins_good_condition = "TRUE"
            elif bins_good_condition.upper() == "FALSE":
                bins_good_condition = "FALSE"

    if down_ramp_good_condition != "":
        down_ramp_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', down_ramp_good_condition)
        if not down_ramp_regex:
            if validation_errors != "":
                validation_errors += ", invalid Down Ramp Good Condition value"
            else:
                validation_errors += "invalid Down Ramp Good Condition value"
        else:
            if down_ramp_good_condition.upper() == "TRUE":
                down_ramp_good_condition = "TRUE"
            elif down_ramp_good_condition.upper() == "FALSE":
                down_ramp_good_condition = "FALSE"

    if other_fixtures_good_condition != "":
        other_fixtures_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', other_fixtures_good_condition)
        if not other_fixtures_regex:
            if validation_errors != "":
                validation_errors += ", invalid Other Fixtures Good Condition value"
            else:
                validation_errors += "invalid Other Fixtures Good Condition value"
        else:
            if other_fixtures_good_condition.upper() == "TRUE":
                other_fixtures_good_condition = "TRUE"
            elif other_fixtures_good_condition.upper() == "FALSE":
                other_fixtures_good_condition = "FALSE"

    if hand_railings_good_condition != "":
        hand_railings_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', hand_railings_good_condition)
        if not hand_railings_regex:
            if validation_errors != "":
                validation_errors += ", invalid Hand Railings Good Condition value"
            else:
                validation_errors += "invalid Hand Railings Good Condition value"
        else:
            if hand_railings_good_condition.upper() == "TRUE":
                hand_railings_good_condition = "TRUE"
            elif hand_railings_good_condition.upper() == "FALSE":
                hand_railings_good_condition = "FALSE"

    if air_con_good_condition != "":
        air_con_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', air_con_good_condition)
        if not air_con_regex:
            if validation_errors != "":
                validation_errors += ", invalid Air Con Good Condition value"
            else:
                validation_errors += "invalid Air Con Good Condition value"
        else:
            if air_con_good_condition.upper() == "TRUE":
                air_con_good_condition = "TRUE"
            elif air_con_good_condition.upper() == "FALSE":
                air_con_good_condition = "FALSE"

    if high_touch_points_good_condition != "":
        high_touch_points_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', high_touch_points_good_condition)
        if not high_touch_points_regex:
            if validation_errors != "":
                validation_errors += ", invalid High Touch Points Good Condition value"
            else:
                validation_errors += "invalid High Touch Points Good Condition value"
        else:
            if high_touch_points_good_condition.upper() == "TRUE":
                high_touch_points_good_condition = "TRUE"
            elif high_touch_points_good_condition.upper() == "FALSE":
                high_touch_points_good_condition = "FALSE"

    if counters_good_condition != "":
        counters_regex = re.search('^(TRUE)|(True)|(true)|(FALSE)|(False)|(false)$', counters_good_condition)
        if not counters_regex:
            if validation_errors != "":
                validation_errors += ", invalid Counters Good Condition value"
            else:
                validation_errors += "invalid Counters Good Condition value"
        else:
            if counters_good_condition.upper() == "TRUE":
                counters_good_condition = "TRUE"
            elif counters_good_condition.upper() == "FALSE":
                counters_good_condition = "FALSE"

    if validation_errors == "":

        message = {
            "message_type": "update",
            "data": {
                "flight_id": "",
                "current_gate": "",
                "terminal": "",
                "flightno": "",
                "direction": "",
                "user_id": "",
                "last_cleaned_start": "",
                "last_cleaned_time": "",
                "cleaning_status": "",
                "ceiling_finishes_good_condition": "",
                "wall_finishes_good_condition": "",
                "floor_finishes_good_condition": "",
                "seats_good_condition": "",
                "bins_good_condition": "",
                "down_ramp_good_condition": "",
                "other_fixtures_good_condition": "",
                "water_cooler_good_condition": "",
                "air_con_good_condition": "",
                "hand_railings_good_condition": "",
                "high_touch_points_good_condition": "",
                "counters_good_condition": ""
            }
        }

        update_record = False

        # update 
        try:
            dynamodb_client = boto3.client('dynamodb')
            dynamodb_resource = boto3.resource('dynamodb')
            flights_table = dynamodb_resource.Table(os.environ["DYNAMODB_FLIGHTS_TABLE"])                           # diva-cleaning-flights
            flights_table_ = os.environ["DYNAMODB_FLIGHTS_TABLE"]                                                   # diva-cleaning-flights
            individual_gates_table = dynamodb_resource.Table(os.environ["DYNAMODB_INDIVIDUAL_GATES_TABLE"])         # diva-cleaning-individual-gates
            individual_gates_table_ = os.environ["DYNAMODB_INDIVIDUAL_GATES_TABLE"]                                 # diva-cleaning-individual-gates

            if cleaning_status in ["CLEANING", "COMPLETE"] or cleaning_status == "":
                # This is the gate and flight_id from the front end application
                response2 = dynamodb_client.query(
                    TableName=flights_table_,
                    KeyConditionExpression='flight_id = :flight_id',
                    ExpressionAttributeValues={':flight_id': {'S': flight_id}}
                )
                
                items = response2['Items']
                log.info("items - {}".format(items))
                if len(items) > 0:
                    item_keys = items[0].keys()

                    current_gate = items[0]['current_gate']['S']
                    direction = items[0]['direction']['S']
                    flightno = items[0]['flightno']['S']
                    scheduled_date = items[0]['scheduled_date']['S']
                    previous_gate = items[0]['previous_gate']['S']
                    arr_dep_flag = items[0]['arr_dep_flag']['S']
                    aircraft_type = items[0]['aircraft_type']['S']
                    display_datetime = items[0]['display_datetime']['S']
                    message_timestamp = items[0]['message_timestamp']['S']
                    unavailable_start = items[0]['unavailable_start']['S']
                    unavailable_end = items[0]['unavailable_end']['S']
                    delete_marker = items[0]['delete_marker']['S']
                    ttl_keys = items[0]['ttl'].keys()
                    if 'S' in ttl_keys:
                        ttl = int(items[0]['ttl']['S'])
                    elif 'N' in ttl_keys:
                        ttl = int(items[0]['ttl']['N'])

                    if current_gate in terminal_3_gates:
                        terminal = "Terminal 3",
                    elif current_gate in terminal_2_gates:
                        terminal = "Terminal 2"
                    elif current_gate in terminal_1_gates:
                        terminal = "Terminal 1"
                    elif current_gate in terminal_4_gates:
                        terminal = "Terminal 4"

                    message = {
                        "message_type": "update",
                        "data": {
                            "flight_id": flight_id,
                            "current_gate": current_gate,
                            "terminal": terminal,
                            "flightno": flightno,
                            "direction": direction,
                            "user_id": user_id_,
                            "last_cleaned_start": last_cleaned_start,
                            "last_cleaned_time": last_cleaned_start if cleaning_status == "CLEANING" else last_cleaned_complete,
                            "cleaning_status": cleaning_status,
                            "ceiling_finishes_good_condition": ceiling_finishes_good_condition,
                            "wall_finishes_good_condition": wall_finishes_good_condition,
                            "floor_finishes_good_condition": floor_finishes_good_condition,
                            "seats_good_condition": seats_good_condition,
                            "bins_good_condition": bins_good_condition,
                            "down_ramp_good_condition": down_ramp_good_condition,
                            "other_fixtures_good_condition": other_fixtures_good_condition,
                            "water_cooler_good_condition": water_cooler_good_condition,
                            "air_con_good_condition": air_con_good_condition,
                            "hand_railings_good_condition": hand_railings_good_condition,
                            "high_touch_points_good_condition": high_touch_points_good_condition,
                            "counters_good_condition": counters_good_condition
                        }
                    }

                    if "cleaning_status" in item_keys:
                        if cleaning_status == 'CLEANING':
                            if items[0]['cleaning_status']['S'] != 'CLEANING':
                                update_record = True
                            else:
                                # return what is stored in the flights table for this flight_id
                                message = {
                                    "message_type": "update",
                                    "data": {
                                        "flight_id": flight_id,
                                        "current_gate": current_gate,
                                        "terminal": terminal,
                                        "flightno": flightno,
                                        "direction": direction,
                                        "user_id": user_id_,
                                        "last_cleaned_start": items[0]['last_cleaned_start']['S'],
                                        "last_cleaned_time": items[0]['last_cleaned_complete']['S'],
                                        "cleaning_status": cleaning_status,
                                        "ceiling_finishes_good_condition": items[0]['ceiling_finishes_good_condition']['S'],
                                        "wall_finishes_good_condition": items[0]['wall_finishes_good_condition']['S'],
                                        "floor_finishes_good_condition": items[0]['floor_finishes_good_condition']['S'],
                                        "seats_good_condition": items[0]['seats_good_condition']['S'],
                                        "bins_good_condition": items[0]['bins_good_condition']['S'],
                                        "down_ramp_good_condition": items[0]['down_ramp_good_condition']['S'],
                                        "other_fixtures_good_condition": items[0]['other_fixtures_good_condition']['S'],
                                        "water_cooler_good_condition": items[0]['water_cooler_good_condition']['S'],
                                        "air_con_good_condition": items[0]['air_con_good_condition']['S'],
                                        "hand_railings_good_condition": items[0]['hand_railings_good_condition']['S'],
                                        "high_touch_points_good_condition": items[0]['high_touch_points_good_condition']['S'],
                                        "counters_good_condition": items[0]['counters_good_condition']['S']
                                    }
                                }
                        else:
                            update_record = True
                    else:
                        update_record = True

                    if update_record:
                        update_expression = 'SET direction = :direction, flightno = :flightno, scheduled_date = :scheduled_date, current_gate = :current_gate, previous_gate = :previous_gate, arr_dep_flag = :arr_dep_flag, aircraft_type = :aircraft_type, display_datetime = :display_datetime, message_timestamp = :message_timestamp, unavailable_start = :unavailable_start, unavailable_end = :unavailable_end, delete_marker = :delete_marker, user_id = :user_id, cleaning_status = :cleaning_status, cleaned_priority = :cleaned_priority, last_cleaned_start = :last_cleaned_start, last_cleaned_complete = :last_cleaned_complete, ceiling_finishes_good_condition = :ceiling_finishes_good_condition, wall_finishes_good_condition = :wall_finishes_good_condition, floor_finishes_good_condition = :floor_finishes_good_condition, water_cooler_good_condition = :water_cooler_good_condition, seats_good_condition = :seats_good_condition, bins_good_condition = :bins_good_condition, down_ramp_good_condition = :down_ramp_good_condition, other_fixtures_good_condition = :other_fixtures_good_condition, hand_railings_good_condition = :hand_railings_good_condition, air_con_good_condition = :air_con_good_condition, high_touch_points_good_condition = :high_touch_points_good_condition, counters_good_condition = :counters_good_condition, #ttl = :ttl'

                        attribute_values = {
                            ':direction' : direction,
                            ':flightno' : flightno,
                            ':scheduled_date' : scheduled_date,
                            ':current_gate' : current_gate,
                            ':previous_gate' : previous_gate,
                            ':arr_dep_flag' : arr_dep_flag,
                            ':aircraft_type' : aircraft_type,
                            ':display_datetime': display_datetime,
                            ':message_timestamp' : message_timestamp,
                            ':unavailable_start' : unavailable_start,
                            ':unavailable_end' : unavailable_end,
                            ':delete_marker' : delete_marker,
                            ':user_id' : user_id_,
                            ':cleaning_status': cleaning_status,
                            ':cleaned_priority': cleaned_priority,
                            ':last_cleaned_start': last_cleaned_start,
                            ':last_cleaned_complete': last_cleaned_complete,
                            ':ceiling_finishes_good_condition': ceiling_finishes_good_condition,
                            ':wall_finishes_good_condition': wall_finishes_good_condition,
                            ':floor_finishes_good_condition': floor_finishes_good_condition,
                            ':water_cooler_good_condition': water_cooler_good_condition,
                            ':seats_good_condition': seats_good_condition,
                            ':bins_good_condition': bins_good_condition,
                            ':down_ramp_good_condition': down_ramp_good_condition,
                            ':other_fixtures_good_condition': other_fixtures_good_condition,
                            ':hand_railings_good_condition': hand_railings_good_condition,
                            ':air_con_good_condition': air_con_good_condition,
                            ':high_touch_points_good_condition': high_touch_points_good_condition,
                            ':counters_good_condition': counters_good_condition,
                            ':ttl' : ttl
                        }

                        attribute_names = {'#ttl' : 'ttl'}

                        try:
                            flights_table.update_item(
                                Key = {'flight_id': flight_id},
                                UpdateExpression = update_expression,
                                ExpressionAttributeValues = attribute_values,
                                ExpressionAttributeNames = attribute_names
                            )
                            log.info('Updated cleaning status for flight id: %s' %(flight_id))

                            response = {'statusCode': 200, 'body': json.dumps(message)}
                        except Exception as e: 
                            log.info("cleaning status was not updated for flight id: {} due to exception.".format(flight_id))
                            log.exception(e)
                            response = {'statusCode': 500, 'body': json.dumps({'ERROR': 'Update failed!'})}
                    else:
                        response = {'statusCode': 200, 'body': json.dumps(message)}

                # Update individual gates table
                priority = 1
                
                response1 = dynamodb_client.query(
                    TableName=individual_gates_table_,
                    KeyConditionExpression='current_gate = :current_gate',
                    ExpressionAttributeValues={':current_gate': {'S': current_gate_}}
                )

                if len(response1['Items']) > 0:
                    items = response1['Items']
                    direction = items[0]["direction"]['S']
                    current_gate = items[0]["current_gate"]['S']
                    flight_id = items[0]["flight_id"]['S']
                    flightno = items[0]["flightno"]['S']
                    arr_dep_flag = items[0]["arr_dep_flag"]['S']
                    display_datetime = items[0]["display_datetime"]['S']
                    aircraft_type = items[0]["aircraft_type"]['S']
                    current_flight_unavailable_start = items[0]["current_flight_unavailable_start"]['S']
                    current_flight_unavailable_end = items[0]["current_flight_unavailable_end"]['S']
                    next_flight_id = items[0]["next_flight_id"]['S']
                    next_flight_display_datetime = items[0]["next_flight_display_datetime"]['S']
                    next_flight_unavailable_start = items[0]["next_flight_unavailable_start"]['S']
                    next_flight_unavailable_end = items[0]["next_flight_unavailable_end"]['S']
                    last_cleaned_time = last_cleaned_start
                    latest_cleaning_status = cleaning_status
                    availability_status = cleaning_status
                    gate_group = items[0]["gate_group"]['S']
                    items_keys = items[0].keys()
                    if 'priority' in items_keys:
                        priority_keys = items[0]['priority'].keys()
                        if 'S' in priority_keys:
                            priority = int(items[0]['priority']['S'])
                        elif 'N' in priority_keys:
                            priority = int(items[0]['priority']['N'])
                    ttl_keys = items[0]['ttl'].keys()
                    if 'S' in ttl_keys:
                        ttl = int(items[0]['ttl']['S'])
                    elif 'N' in ttl_keys:
                        ttl = int(items[0]['ttl']['N'])

                    update_expression = 'SET direction = :direction, flight_id = :flight_id, flightno = :flightno, arr_dep_flag = :arr_dep_flag, display_datetime = :display_datetime, aircraft_type = :aircraft_type, current_flight_unavailable_start = :current_flight_unavailable_start, current_flight_unavailable_end = :current_flight_unavailable_end, next_flight_id = :next_flight_id, next_flight_display_datetime = :next_flight_display_datetime, next_flight_unavailable_start = :next_flight_unavailable_start, next_flight_unavailable_end = :next_flight_unavailable_end, last_cleaned_time = :last_cleaned_time, latest_cleaning_status = :latest_cleaning_status, availability_status = :availability_status, gate_group = :gate_group, priority = :priority, #ttl = :ttl'

                    attribute_values = {
                        ':direction': direction,
                        ':flight_id': flight_id,
                        ':flightno': flightno,
                        ':arr_dep_flag': arr_dep_flag,
                        ':display_datetime': display_datetime, 
                        ':aircraft_type': aircraft_type,
                        ':current_flight_unavailable_start': current_flight_unavailable_start,
                        ':current_flight_unavailable_end': current_flight_unavailable_end,
                        ':next_flight_id': next_flight_id,
                        ':next_flight_display_datetime': next_flight_display_datetime,
                        ':next_flight_unavailable_start': next_flight_unavailable_start,
                        ':next_flight_unavailable_end': next_flight_unavailable_end,
                        ':last_cleaned_time': last_cleaned_time,
                        ':latest_cleaning_status': latest_cleaning_status,
                        ':availability_status': availability_status,
                        ':gate_group': gate_group,
                        ':priority': priority,
                        ':ttl': ttl
                    }

                    attribute_names = {'#ttl' : 'ttl'}

                    try:
                        individual_gates_table.update_item(
                            Key = {'current_gate': current_gate},
                            UpdateExpression = update_expression,
                            ExpressionAttributeValues = attribute_values,
                            ExpressionAttributeNames = attribute_names
                        )
                        log.info('Updated cleaning status for flight id: %s' %(flight_id))
                        #response = {'statusCode': 200, 'body': json.dumps({'status': 200})}
                    except Exception as e: 
                        log.info("cleaning status was not updated for flight id: {} due to exception.".format(flight_id))
                        log.exception(e)
                        response = {'statusCode': 500, 'body': json.dumps({'ERROR': 'Update failed!'})}

        except Exception as e:
            log.error('Internal error when trying to update this flight id and it was skipped: %s' %(flight_id))
            log.exception(e)
            response = {'statusCode': 500, 'body': json.dumps({'ERROR': 'Update failed!'})}
    else:
        response = {'statusCode': 400, 'body': json.dumps({'ERROR': validation_errors})}
        
    return response