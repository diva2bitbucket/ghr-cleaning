import os
import glob
import json
import gzip
import boto3
import logging
import pandas as pd
import numpy as np
from io import StringIO
from subprocess import call
from datetime import datetime, timedelta, date

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

source_s3_bucket_name = os.environ["SOURCE_S3_BUCKET_NAME"]                                                 # diva-prod-cleaning-logs
result_s3_bucket_name = os.environ["RESULT_S3_BUCKET_NAME"]                                                 # diva-prod-cleaning-logs-extraction-results
search_prefix = os.environ["SEARCH_PREFIX"].strip()                                                         # flights
search_date_range = int(os.environ["SEARCH_DATE_RANGE"].strip())                                            # ex. 7
search_end_datetime_ = os.environ["SEARCH_END_DATETIME"].strip()                                            # ex. 2020-08-31 23:59:59 or "-"

if search_end_datetime_ != "-":
    search_end_datetime = datetime.strptime(os.environ["SEARCH_END_DATETIME"].strip(), "%Y-%m-%d %H:%M:%S")     # ex. 2020-08-31 23:59:59 or "-"
    search_date = search_end_datetime - timedelta(days=search_date_range)
else:
    # use the date today instead
    search_date = datetime.now() - timedelta(days=search_date_range)
    search_end_datetime = datetime.now()

DOWNLOAD_LOCATION_PATH = "/tmp/"
keys_list = []
list_of_files = []

s3_resource = boto3.resource("s3")
s3_client = boto3.client("s3")


def lambda_handler(event, context):

    delete_s3_objects()
    process_gzip_files()


def save_to_dest_s3(processed_data_df):
    '''
        Save the processed file into the destination S3 bucket:
            1. Create a buffer to save the file temporarily
            2. Convert the DataFrame to CSV file
            3. Save the CSV file into the bucket 
        Destination S3 bucket: "diva-redshift-temp"
    '''
    from_date = search_date.strftime("%Y-%m-%d")
    #to_date = date.today().strftime("%Y-%m-%d")
    to_date = search_end_datetime.strftime("%Y-%m-%d")

    csv_filename = 'where2clean_flights_data_' + from_date + '_to_' + to_date + '.csv'
    
    # Save the file into a buffer first 
    csv_buffer = StringIO()
    
    # Convert dataframe to CSV
    processed_data_df.to_csv(csv_buffer, sep=',', index=False)

    # Save CSV into destination S3 bucket
    s3_resource.Object(result_s3_bucket_name, csv_filename).put(Body=csv_buffer.getvalue())

    log.info('File "%s" is saved into S3 bucket %s' %(csv_filename, result_s3_bucket_name))    


def local_directory():
    '''
    1. Create a local directory inside Lambda to store the ZIP files temporarily 
    2. Usually temporary files are stored in "/tmp" location within Lambda 
    '''
        
    # Temporary use of non-persistent disk space in Lambda's own '/tmp' directory for downloading files from source S3

    if not os.path.exists(DOWNLOAD_LOCATION_PATH):
        log.info("Creating local directory /tmp to store the files")
        os.mkdir(DOWNLOAD_LOCATION_PATH)
    else:
        log.info('tmp directory already exists, clean it up before re-using')
        call('rm -rf /tmp/*', shell=True)


def download_files_from_source_s3():
    # Create local directory inside Lambda if it does not exist 
    local_directory()
        
    key_str_list = []

    # Connect to S3 bucket
    source_s3_bucket = s3_resource.Bucket(source_s3_bucket_name)

    flights_ = "flights"
    search_year = search_date.strftime("%Y")
    search_month = search_date.strftime("%m")
    search_day = search_date.strftime("%d")
    search_hour = search_date.strftime("%H") + '/'

    search_start_after = os.path.join(flights_, search_year, search_month, search_day, search_hour)

    # Create a reusable Paginator to search for more than 1000 objects
    paginator = s3_client.get_paginator('list_objects_v2')

    # Create a PageIterator from the Paginator 
    page_iterator = paginator.paginate(Bucket=source_s3_bucket_name, Prefix=search_prefix, StartAfter=search_start_after) 
    
    for page in page_iterator:
        if 'Contents' in page:  # To check if the bucket has at least one object
            for obj in page['Contents']:
                key_str = obj['Key']

                # Look for keys that ends with .csv
                if key_str.endswith(".gz"):

                    key_str_filename = str.split(str.split(key_str)[0], '/')[-1]

                    key_str_list = key_str.split("/")
                    # ex. flights/2020/08/19/14/diva-cleaning-flights-delivery-stream-2-2020-08-19-14-57-13-bbae6eaa-f3af-486b-b5d0-dc846845826d.gz
                    file_dt = key_str_list[1] + "-" + key_str_list[2] + "-" + key_str_list[3] + " " + key_str_list[4] + ":00:00"
                    file_date = datetime.strptime(file_dt, "%Y-%m-%d %H:%M:%S")

                    if file_date <= search_end_datetime:
                        csv_temp_path = DOWNLOAD_LOCATION_PATH + key_str_filename

                        # Download S3 object to a file
                        source_s3_bucket.meta.client.download_file(source_s3_bucket_name, key_str, csv_temp_path)
                        
                        # Append all the ZIP files to a list
                        keys_list.append(obj['Key'])
                        list_of_files = glob.glob(os.path.join(DOWNLOAD_LOCATION_PATH, "*"))
        else:
            # No objects found
            log.info('No files found in source S3 bucket')
    
    return list_of_files


def process_gzip_files():
    
    new_image = {
        'flight_id': '',
        'direction': '', 
        'flightno': '', 
        'current_gate': '', 
        'arr_dep_flag': '', 
        'unavailable_start': '', 
        'display_datetime': '', 
        'unavailable_end': '', 
        'scheduled_date': '', 
        'previous_gate': '', 
        'delete_marker': '', 
        'aircraft_type': '', 
        'message_timestamp': '',
        'user_id': '',
        'cleaning_status': '',
        'last_cleaned_start': '',
        'last_cleaned_complete': '',
        'hand_railings_good_condition': '',
        'ceiling_finishes_good_condition': '',
        'seats_good_condition': '',
        'floor_finishes_good_condition': '',
        'wall_finishes_good_condition': '',
        'air_con_good_condition': '',
        'bins_good_condition': '',
        'water_cooler_good_condition': '',
        'down_ramp_good_condition': '',
        'other_fixtures_good_condition': ''
    }
    ni_list = []

    list_of_gzip_files = download_files_from_source_s3()

    for gzip_file in list_of_gzip_files:

        gzipfile = open(gzip_file, 'rb')
        magic_header = gzipfile.read(2)
        if magic_header == b'\x1f\x8b':
            gzipfile.seek(0)
            archive = gzip.GzipFile(fileobj=gzipfile)
            file_content = archive.read().decode("utf-8")
        else:
            gzipfile.seek(0)
            file_content = gzipfile.read().decode("utf-8")

        if len(file_content) > 0:
            ni = 0
            has_records = True
            while has_records:
                fc = file_content[ni:]
                bs = fc.find("{")
                be = fc.find("}") + 1
                n_i = json.loads(fc[bs:be])
                n_i_keys = n_i.keys()

                new_image['flight_id'] = n_i['flight_id']
                new_image['direction'] = n_i['direction'] 
                new_image['flightno'] = n_i['flightno']
                new_image['current_gate'] = n_i['current_gate']
                new_image['arr_dep_flag'] = n_i['arr_dep_flag']

                if 'unavailable_start' in n_i_keys:
                    new_image['unavailable_start'] = n_i['unavailable_start']

                new_image['display_datetime'] = n_i['display_datetime']

                if 'unavailable_end' in n_i_keys:
                    new_image['unavailable_end'] = n_i['unavailable_end']

                new_image['scheduled_date'] = n_i['scheduled_date']
                new_image['previous_gate'] = n_i['previous_gate']
                new_image['delete_marker'] = n_i['delete_marker']
                new_image['aircraft_type'] = n_i['aircraft_type']
                new_image['message_timestamp'] = n_i['message_timestamp']

                if 'user_id' in n_i_keys:
                    new_image['user_id'] = n_i['user_id']
                if 'cleaning_status' in n_i_keys:
                    new_image['cleaning_status'] = n_i['cleaning_status']
                if 'last_cleaned_start' in n_i_keys:
                    new_image['last_cleaned_start'] = n_i['last_cleaned_start']
                if 'last_cleaned_complete' in n_i_keys:
                    new_image['last_cleaned_complete'] = n_i['last_cleaned_complete']
                if 'hand_railings_good_condition' in n_i_keys:
                    new_image['hand_railings_good_condition'] = n_i['hand_railings_good_condition']
                if 'ceiling_finishes_good_condition' in n_i_keys:
                    new_image['ceiling_finishes_good_condition'] = n_i['ceiling_finishes_good_condition']
                if 'seats_good_condition' in n_i_keys:
                    new_image['seats_good_condition'] = n_i['seats_good_condition']
                if 'floor_finishes_good_condition' in n_i_keys:
                    new_image['floor_finishes_good_condition'] = n_i['floor_finishes_good_condition']
                if 'wall_finishes_good_condition' in n_i_keys:
                    new_image['wall_finishes_good_condition'] = n_i['wall_finishes_good_condition']
                if 'air_con_good_condition' in n_i_keys:
                    new_image['air_con_good_condition'] = n_i['air_con_good_condition']
                if 'bins_good_condition' in n_i_keys:
                    new_image['bins_good_condition'] = n_i['bins_good_condition']
                if 'water_cooler_good_condition' in n_i_keys:
                    new_image['water_cooler_good_condition'] = n_i['water_cooler_good_condition']
                if 'down_ramp_good_condition' in n_i_keys:
                    new_image['down_ramp_good_condition'] = n_i['down_ramp_good_condition']
                if 'other_fixtures_good_condition' in n_i_keys:
                    new_image['other_fixtures_good_condition'] = n_i['other_fixtures_good_condition']

                ni_list.append(new_image.copy())

                new_image = {
                    'flight_id': '',
                    'direction': '', 
                    'flightno': '', 
                    'current_gate': '', 
                    'arr_dep_flag': '', 
                    'unavailable_start': '', 
                    'display_datetime': '', 
                    'unavailable_end': '', 
                    'scheduled_date': '', 
                    'previous_gate': '', 
                    'delete_marker': '', 
                    'aircraft_type': '', 
                    'message_timestamp': '',
                    'user_id': '',
                    'cleaning_status': '',
                    'last_cleaned_start': '',
                    'last_cleaned_complete': '',
                    'hand_railings_good_condition': '',
                    'ceiling_finishes_good_condition': '',
                    'seats_good_condition': '',
                    'floor_finishes_good_condition': '',
                    'wall_finishes_good_condition': '',
                    'air_con_good_condition': '',
                    'bins_good_condition': '',
                    'water_cooler_good_condition': '',
                    'down_ramp_good_condition': '',
                    'other_fixtures_good_condition': ''
                }

                ni = file_content.find("{", ni+be)
                if (ni == -1):
                    has_records = False

    if len(ni_list) > 0:
        ni_df = pd.DataFrame.from_records(ni_list, columns=['flight_id', 'direction', 'flightno', 'current_gate', 'arr_dep_flag', 'unavailable_start', 'display_datetime', 'unavailable_end', 'scheduled_date', 'previous_gate', 'delete_marker', 'aircraft_type', 'message_timestamp', 'user_id', 'cleaning_status', 'last_cleaned_start', 'last_cleaned_complete', 'hand_railings_good_condition', 'ceiling_finishes_good_condition', 'seats_good_condition', 'floor_finishes_good_condition', 'wall_finishes_good_condition', 'air_con_good_condition', 'bins_good_condition', 'water_cooler_good_condition', 'down_ramp_good_condition', 'other_fixtures_good_condition'])
        save_to_dest_s3(ni_df)


def list_s3_objects():
    s3_objects_list = []

    # Create a reusable Paginator to search for more than 1000 objects
    paginator = s3_client.get_paginator('list_objects_v2')

    # Create a PageIterator from the Paginator 
    page_iterator = paginator.paginate(Bucket=result_s3_bucket_name) 
    
    for page in page_iterator:
        if 'Contents' in page:  # To check if the bucket has at least one object
            for obj in page['Contents']:
                key_str = obj['Key']

                s3_objects_list.append({"Key": key_str})

    return s3_objects_list


def delete_s3_objects():

    objects_list = list_s3_objects()

    if len(objects_list) > 0:
        s3_client.delete_objects(
            Bucket=result_s3_bucket_name,
            Delete={
                'Objects': objects_list,
                'Quiet': True
            }
        )