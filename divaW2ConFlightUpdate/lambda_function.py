import logging
import boto3
import json
import os
import time
import traceback
from datetime import datetime, timedelta
from botocore.exceptions import ClientError

log = logging.getLogger()
if log.handlers:
    HANDLER = log.handlers[0]
else:
    HANDLER = logging.StreamHandler()
    log.addHandler(HANDLER)
log_format = '[%(levelname)s] %(asctime)s- %(message)s (File %(pathname)s, Line %(lineno)s)'
HANDLER.setFormatter(logging.Formatter(log_format))
log.setLevel(logging.INFO)

firehose_delivery_stream = os.environ['FIREHOSE_DELIVERY_STREAM']                           # diva-cleaning-flights-delivery-stream
individual_gates_ttl_duration = int(os.environ['INDIVIDUAL_GATES_TTL_DURATION'])            # 259200 sec
flights_table = os.environ["DYNAMODB_FLIGHTS_TABLE"]                                        # diva-cleaning-flights
flights_table_current_gate_index = os.environ['DYNAMODB_FLIGHTS_TABLE_CURRENT_GATE_INDEX']  # diva-cleaning-flights-current_gate-index
individual_gates_table = os.environ["DYNAMODB_INDIVIDUAL_GATES_TABLE"]                      # diva-cleaning-individual-gates
gates_table = os.environ["DYNAMODB_GATES_TABLE"]                                            # diva-cleaning-gates
mapping_files_bucket_name = os.environ['MAPPING_FILES_BUCKET_NAME']                         # diva-dev-cleaning-mapping-files
gates_zone_list_file = os.environ['GATES_ZONE_LIST_FILE']                                   # gates_zone_list.json

dynamodb_resource = boto3.resource('dynamodb')
dynamodb_client = boto3.client("dynamodb")
s3_client = boto3.client("s3")

gates_table_ = dynamodb_resource.Table(gates_table)
individual_gates_table_ = dynamodb_resource.Table(individual_gates_table)

s3_object = s3_client.get_object(Bucket=mapping_files_bucket_name, Key=gates_zone_list_file)
gates_zone_list = json.loads(s3_object['Body'].read().decode('utf-8'))

temp_individual_gate = {}
gate_record = {}
dependencies_list = []
datetime_format = "%Y-%m-%d %H:%M:%S"


def lambda_handler(event, context):
    log.info('event: ' + json.dumps(event, indent=2))
    
    # Compact batches - remove redundant batches or batches with deleteRecords
    event_records = compact_event(event)
    log.info('event_records: {}'.format(event_records))
    if event_records == []:
        log.info("Zero event records, aborting")
        return

    try:
        for record in event_records:
            record = json.dumps(record['dynamodb']['NewImage']).encode("utf-8")
            sendBatchToFirehose(record, firehose_delivery_stream)
    except Exception as e:
        log.error("Error while sending to firehose, logging details - records: {}, record: {}, exception: {}".format(event_records, record, e))
        
    update_gates_table(event_records)


def sendBatchToFirehose(blob, firehose_delivery_stream):
    """
    Wrapper to dump things in firehose
    :blob dict: json from dynamodb to dump in s3 via firehose
    :firehose_delivery_stream string: name of the kinesis data firehose
    :return bool: bool describing status of api call - successful=True
    """
    firehose = boto3.client('firehose')
    try:
        response = firehose.put_record_batch(
            DeliveryStreamName=firehose_delivery_stream,
            Records=[
                {
                    'Data': blob
                },
            ]
        )
    except Exception as e:
        log.error("Event logging faced error: {}".format(e))
        return False
    else:
        log.info("Event logged without error: {}".format(response))
        return True


def compact_event(event):
    """
    Compacts events - remove all deleted records (if there is any)
    :event dict: event object received by lambda
    :return event_records: compacted events to pass to front end
    """
    event_records = list(filter(lambda x: x["eventName"] != "REMOVE", event["Records"]))

    compacted_events = []
    for event_record in event_records:
        dynamodb_keys = event_record['dynamodb'].keys()
        if 'NewImage' in dynamodb_keys:
            for field in event_record['dynamodb']["NewImage"]:
                if "S" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = event_record['dynamodb']["NewImage"][field]["S"]
                elif "N" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = int(event_record['dynamodb']["NewImage"][field]["N"])
                elif "NULL" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = ""
                elif "BOOL" in event_record['dynamodb']["NewImage"][field]:
                    event_record['dynamodb']["NewImage"][field] = event_record['dynamodb']["NewImage"][field]["BOOL"]
        if 'OldImage' in dynamodb_keys:
            for field in event_record['dynamodb']["OldImage"]:
                if "S" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = event_record['dynamodb']["OldImage"][field]["S"]
                elif "N" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = int(event_record['dynamodb']["OldImage"][field]["N"])
                elif "NULL" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = ""
                elif "BOOL" in event_record['dynamodb']["OldImage"][field]:
                    event_record['dynamodb']["OldImage"][field] = event_record['dynamodb']["OldImage"][field]["BOOL"]
        compacted_events.append(event_record)

    return compacted_events


def update_gates_table(event_records):
    """
    Update gates dynamodb table
    :event_records: compacted event records, returned by compact_event method
    1. create a list (array) of unique combinations of current_gate and direction; source data is event_records
    2. loop through the list
    3. for each unique combination, query flights dynamodb table for records with the same current_gate and direction
    4. if query result is empty, delete the corresponding gates table record
    5. otherwise, update gates table, which calls update_gates_record method
    """
    if len(event_records) > 0:

        records_list = []
        distinct_list = []
        
        for item in event_records:
            dynamodb_keys = item['dynamodb'].keys()
            if 'NewImage' in dynamodb_keys:
                records_list.append(item['dynamodb']['NewImage'])
            if 'OldImage' in dynamodb_keys:
                records_list.append(item['dynamodb']['OldImage'])

        current_gates_list = list(filter(lambda x: x["current_gate"] != "", records_list))

        if len(current_gates_list) > 0:
            for cg in current_gates_list:
                if cg['current_gate'] not in distinct_list:
                    distinct_list.append(cg['current_gate'])

        previous_gates_list = list(filter(lambda x: x["previous_gate"] != "", records_list))

        if len(previous_gates_list) > 0:
            for pg in previous_gates_list:
                if pg['previous_gate'] not in distinct_list:
                    distinct_list.append(pg['previous_gate'])

        if len(distinct_list) > 0:

            #priority = 1
            query_results = []
            current_datetime = datetime.now() + timedelta(hours=8)

            for gate_ in distinct_list:

                attribute_values = {':current_gate': {'S': gate_}, ':N': {'S': 'N'}}

                response = dynamodb_client.query(
                    TableName=flights_table,
                    IndexName=flights_table_current_gate_index,
                    KeyConditionExpression='current_gate = :current_gate',
                    ExpressionAttributeValues=attribute_values,
                    FilterExpression='delete_marker = :N'
                )

                if len(response['Items']) > 0:
                    items = response['Items']

                    for item in items:
                        query_results.append(item)

                    query_records = sorted(query_results, key = lambda x: x["unavailable_end"]['S'], reverse=True)

                    temp_individual_gate = {
                        'current_gate': gate_,
                        'direction': '',
                        'flight_id': '',
                        'flightno': '',
                        'arr_dep_flag': '',
                        'display_datetime': '', 
                        'aircraft_type': '',
                        'current_flight_unavailable_start': '',
                        'current_flight_unavailable_end': '',
                        'next_flight_id': '',
                        'next_flight_display_datetime': '',
                        'next_flight_unavailable_start': '',
                        'next_flight_unavailable_end': '',
                        'next_flight_aircraft_type': '',
                        'last_cleaned_time': '',
                        'latest_cleaning_status': 'CLEANED',
                        'availability_status': '',
                        'gate_group': '-',
                        'ttl' : int(time.time() + individual_gates_ttl_duration)
                    }

                    for flight in query_records:
                        if 'last_cleaned_complete' in flight:
                            if flight['last_cleaned_complete']['S'] != "" and flight['cleaning_status']['S'] == 'COMPLETE':
                                temp_individual_gate['last_cleaned_time'] = flight['last_cleaned_complete']['S'] if flight['cleaning_status']['S'] == 'COMPLETE' else ""
                                break

                        log.info("flight - {}".format(flight))
                        if 'last_cleaned_complete' not in flight or flight['last_cleaned_complete']['S'] == "":
                            temp_individual_gate['next_flight_id'] = temp_individual_gate['flight_id']
                            temp_individual_gate['next_flight_display_datetime'] = temp_individual_gate['display_datetime']
                            temp_individual_gate['next_flight_unavailable_start'] = temp_individual_gate['current_flight_unavailable_start']
                            temp_individual_gate['next_flight_unavailable_end'] = temp_individual_gate['current_flight_unavailable_end']
                            temp_individual_gate['next_flight_aircraft_type'] = temp_individual_gate['aircraft_type']
                            temp_individual_gate['direction'] = flight['flight_id']['S'][:3]
                            temp_individual_gate['flight_id'] = flight['flight_id']['S']
                            temp_individual_gate['flightno'] = flight['flightno']['S']
                            temp_individual_gate['arr_dep_flag'] = flight['arr_dep_flag']['S']
                            temp_individual_gate['aircraft_type'] = flight['aircraft_type']['S']
                            temp_individual_gate['display_datetime'] = flight['display_datetime']['S']
                            temp_individual_gate['current_flight_unavailable_start'] = flight['unavailable_start']['S']
                            temp_individual_gate['current_flight_unavailable_end'] = flight['unavailable_end']['S']

                            current_flight_unavailable_start = datetime.strptime(temp_individual_gate['current_flight_unavailable_start'], datetime_format)
                            current_flight_unavailable_end = datetime.strptime(temp_individual_gate['current_flight_unavailable_end'], datetime_format)

                            if current_datetime < current_flight_unavailable_end:
                                temp_individual_gate['latest_cleaning_status'] = 'NOT AVAILABLE'
                                temp_individual_gate['availability_status'] = 'NOT AVAILABLE - CURRENT FLIGHT'
                            else:
                                if temp_individual_gate['arr_dep_flag'] == 'N':
                                    temp_individual_gate['latest_cleaning_status'] = 'NOT AVAILABLE'
                                    temp_individual_gate['availability_status'] = 'NOT AVAILABLE - CURRENT FLIGHT'
                                elif temp_individual_gate['arr_dep_flag'] == 'Y':
                                    temp_individual_gate['latest_cleaning_status'] = 'AVAILABLE'
                                    temp_individual_gate['availability_status'] = 'AVAILABLE'

                            if current_datetime > current_flight_unavailable_start:
                                break

                    if temp_individual_gate['latest_cleaning_status'] == 'CLEANED':
                        temp_individual_gate['availability_status'] = 'CLEANED'
                        temp_individual_gate['priority'] = 1

                    if temp_individual_gate['flight_id'] != '':
                        log.info("temp_individual_gate - {}".format(temp_individual_gate))
                        set_gate_group_value(temp_individual_gate)
                    
                    update_individual_gates_table(temp_individual_gate)
                else:
                    try:
                        individual_gates_table_.delete_item(
                            Key={'current_gate': gate_}
                        )
                        log.info('Deleted DynamoDB Individual Gate record: %s' %(json.dumps({'current_gate': gate_})))
                        gates_table_.delete_item(
                            Key={'current_gate': gate_}
                        )
                        log.info('Deleted DynamoDB Gate record: %s' %(json.dumps({'current_gate': gate_})))
                    except dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
                        log.exception(ccfe)
                        pass
                    except Exception:
                        log.error('Internal error when trying to delete this record and it was skipped: %s' %(json.dumps({'current_gate': gate_})))
                        log.error(traceback.format_exc())
                query_results = []


def set_gate_group_value(temp_individual_gate):

    gate_group = ""
    
    if len(gates_zone_list) > 0:
        for gate_info in gates_zone_list:
            if gate_info['gate'] == temp_individual_gate['current_gate']:
                gate_group = gate_info['gate_group']
                if gate_group == "":
                    temp_individual_gate['gate_group'] = "-"
                else:
                    temp_individual_gate['gate_group'] = gate_group


def update_individual_gates_table(update_individual_gate):
	
    try:
        update_expression = 'SET '
        attribute_values = {}
        attribute_names = {}
        counter = 1

        for key in update_individual_gate:
            if key != 'current_gate':
                var = ':var' + str(counter)
				
                # To handle updates of DynamoDB for using 'ttl' as a reserved name
                if key == 'ttl':
                    new_key = '#ttl'
                    attribute_names[new_key] = key
                    update_expression = update_expression + new_key + ' = ' + var + ', '
                else:
                    update_expression = update_expression + key +  ' = ' + var + ', '
					
                attribute_values[var] = update_individual_gate[key]
                counter += 1

        attribute_values[':cleaning'] = 'CLEANING'
        attribute_values[':current_flight_id'] = update_individual_gate['flight_id']
        condition_expression = "(latest_cleaning_status <> :cleaning OR flight_id <> :current_flight_id) OR attribute_not_exists(latest_cleaning_status)"
        update_expression = update_expression[:-2]
        try:
            individual_gates_table_.update_item(
                Key = {'current_gate': update_individual_gate['current_gate']},
                UpdateExpression = update_expression,
                ConditionExpression = condition_expression,
                ExpressionAttributeValues = attribute_values,
                ExpressionAttributeNames = attribute_names)

            log.info('Updated data into DynamoDB Individual Gate table for this record: %s' %(json.dumps(update_individual_gate)))

        except dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as ccfe:
            log.exception(ccfe)
            pass
    except:
        log.error('Internal error when trying to update this record and it was skipped: %s' %(json.dumps(update_individual_gate)))
        log.error(traceback.format_exc())

    return True